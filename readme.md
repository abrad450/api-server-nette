# API Server for Nette Framework


Currently JSON RPC (and JSON REST experimental!) only

## Registration

Register extension in NEON file:

    extensions:
        apiServer: AbraD450\ApiServer\DIExtension       # This extension
        mapped: AbraD450\MappedDatabase\DIExtension     # MappedDatabase extension is REQUIRED for this extension to run

...and configure the extension:

    apiServer:
        query:
            maxPageSize: 500
            dateTimeFormat: DATE_ISO8601    # Format for DateTimeInterface objects
            timeFormat: '%H:%M:%S'          # Format for DateInterval objects
            maxPageSize: 100                # Max page size for selections
            pagination: 'auto'              # for selections - TRUE (always visible), FALSE (never visible), 'auto' - depends on pageSize settings
        rpc:
            endpointname:
                namespace: App\ApiModule
                accessControl:
                    allowedOrigins: ['*']
                    allowCredentials: true
                    allowMethods: ['POST', 'OPTIONS']
                    allowHeaders: ['origin', 'content-type', 'accept', 'x-tracy-ajax']
                    exposeHeaders: ['origin', 'location', 'content-type', 'accept', 'x-tracy-ajax']
                    maxAge: 1728000
        rest:   # experimental
            endpointprefix:
                namespace: App\V1Module
                accessControl:
                    allowedOrigins: ['*']
                    allowCredentials: true
                    allowMethods: ['POST', 'PUT', 'GET', 'DELETE', 'OPTIONS']
                    allowHeaders: ['authorization', 'origin', 'content-type', 'accept', 'x-tracy-ajax']
                    exposeHeaders: ['origin', 'location', 'content-type', 'accept', 'x-tracy-ajax']
                    maxAge: 1728000                


The "endpointname" is the endpoint (it would be for example **http://localhost:8088/endpointname**).

Register all API Controllers from defined namespace as services: 

	services:
	    - App\ApiModule\UserController

This is the Controller itself:

	<?php
	namespace App\ApiModule;
	
	use AbraD450\ApiServer\ApiController;

	class UserController extends ApiController
	{
	    public function read()
	    {
	        return [['id' => 1], ['id' => 2], ['id' => 3]];
	    }
	}

## Usage example

JSON RPC use only on **POST** requests:

	POST http://localhost:8088/endpointname

Request body

	{
	    "jsonrpc": "2.0",
	    "method": "user.read",
	    "id": 1
	}

Response body

	{
	    "jsonrpc": "2.0",
	    "result": [
	        {
	            "id": 1
	        },
	        {
	            "id": 2
	        },
	        {
	            "id": 3
	        }
	    ],
	    "id": 1
	}


# API Query

## Query processor service

Once extension is registered, there is Query Processor service available that can be injected:
    
    /**
     * Query Processor
     *
     * @var AbraD450\ApiQuery\Processor
     * @inject
     */
    public $processor;

This service proceses API Query requests with the following formats:

## Formats of API Query

The short format (can only filter/sort/search/page base Selection)

    {
        "select": "id,url,name,createdStamp,createdUserId,deleted",
        "search": "blog",
        "page": 1,
        "pageSize": 50,
        "sort": [{"field": "id"}],
        "filter": {"logic": "and", "filters": [{"field": "name", "operator": "containing", "value": "blog"}]}
    }

Extended format (can filter/sort/search/page also nested Selections)

    {
        "select": "id,url,name,createdStamp,createdUserId,createdUser(id,email,password,firstName,lastName,active,locked,posts(id,parentId,pos,name,slug,perex)),deleted,time",
        "criteria": {
            "@": {
                "search": "blog",
                "page": 1,
                "pageSize": 50,
                "sort": [{"field": "id"}, {"field": "name", "dir": "desc"}],
                "filter": {"logic": "and", "filters": [{"field": "metaTitle", "operator": "contains", "value": "a"}, {"field": "metaTitle", "operator": "contains", "value": "e"}, {"logic": "or", "filters": [{"field": "metaTitle", "operator": "contains", "value": "i"}, {"field": "metaTitle", "operator": "contains", "value": "o"}]} ]}
            },
            "createdUser/posts": {
                "page": 1,
                "pageSize": 2,
                "filter": {"filters": [{"field": "name", "operator": "contains", "value": "a"},{"field": "deleted", "value": 0}]}
            }
        }
    }

- **The @ criteria is automatically created when used the short format.**
- The @ criteria is for the base Selection and other criteria is for the nested Selections

## The Query object

The API Queries above can be parsed to *objects* or *arrays* (json_decode) and used like this:

    // decode JSON mentioned above
    $data = json_decode($json);

    // get Selection to be Queried
    $sites = $this->siteManager->get();

    // create Query object
    $query = AbraD450\ApiQuery\Query\Query::create($data);

    // create array structure from given Selection using the Query
    // (the $this->processor service is mentioned above)
    $struct = $this->processor->process($sites, $query);

    // echo the structure as JSON string to the output
    echo json_encode($struct);

## Events

If you need to modify the Query or decide whether or not to send some Entity to the output, there is a couple of events availble on the Processor service for this purpose:


- **onSelection** - called whenever any Selection is about to be processed. Can be used to add ->where calls for example to add ACL conditions

- **onEntity** - called whenever any Entity is about to be processed. Can be used to prevent referenced (or related) object to be send to output.

The callback in both cases receive Selection/Entity as a first parameter and ProcessorEvent object as a second parameter.

If you need to get rid of the Selection or Entity in both cases set **ProcessorEvent::$shouldSkip** parameter to TRUE inside the callback.