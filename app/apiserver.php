<?php

$autoloader = require __DIR__ . '/../src/composer_autoloader.php';

if (!$autoloader()) {
    die(
        'You need to set up the project dependencies using the following commands:' . PHP_EOL .
        'curl -sS https://getcomposer.org/installer | php' . PHP_EOL .
        'php composer.phar install' . PHP_EOL
    );
}

$getBootstrapClass = function() {
    $paths = [
        __DIR__.'/../../../../index.php',
        __DIR__.'/../../../../www/index.php',
        __DIR__.'/../../../../Www/index.php',
        __DIR__.'/../../../../app/Bootstrap.php',
        __DIR__.'/../../../../App/Bootstrap.php',
        __DIR__.'/../../../../app/bootstrap.php',
        __DIR__.'/../../../../App/bootstrap.php'
    ];
    foreach($paths as $path) {
        if(!is_file($path)) {
            continue;
        }
        $contents = file_get_contents($path);
        $matches = null;
        if(preg_match('/^.*=\s*(.*Bootstrap)::boot\(\);.*$/iUm', $contents, $matches)) {
            return trim($matches[1]);
        }
        if(preg_match('/class Bootstrap/iUm', $contents)) {
            if(preg_match('/^\s*namespace\s+([^\s]+)\s*;\s*$/iUm', $contents, $matches)) {
                return trim($matches[1]).'\\Bootstrap';
            }
        }
    }
    die("\nUnable to find the 'App\\Bootstrap' class or its equivalent on following paths:\n - ".implode("\n - ", $paths)."\n");
};

$configurator = $getBootstrapClass()::boot();
$container = $configurator->createContainer();
$openApiGenerator = $container->getByType(AbraD450\ApiServer\REST\Documentation\OpenAPIGenerator::class);

return new \AbraD450\ApiServer\Console\ApiServerApplication($openApiGenerator);
