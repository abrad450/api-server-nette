<?php

namespace AbraD450\ApiServer;

use Nette;

/**
 * Api Controller
 */
abstract class ApiController
{
    use Nette\SmartObject;
    
    /**
     * Flag indicating that startup was called
     */
    private bool $startupCalled = false;
    
    /**
     * Flag indicating that primary Controller dependencies ere injected
     */
    private bool $primaryInjected = false;
    
    /**
     * Flag indicating that parameters was set (by the server)
     */
    private bool $parametersSet = false;
    
    /**
     * HTTP Request
     */
    protected Nette\Http\IRequest $request;
    
    /**
     * HTTP Response
     */
    protected Nette\Http\IResponse $response;
    
    /**
     * User
     */
    protected Nette\Security\User $user;
    
    /**
     * Session
     */
    protected Nette\Http\Session $session;
    
    /**
     * Session
     */
    protected Query\Processor $processor;
    
    /**
     * Link generator
     */
    protected Nette\Application\LinkGenerator $linkGenerator;
    
    /**
     * Called controller name
     */
    protected string $controllerName;
    
    /**
     * Called controller service name
     */
    protected string $serviceName;
    
    /**
     * Called method name
     */
    protected string $methodName;
    
    /**
     * Parameters
     */
    protected array $parameters;
    
    /**
     * JSON data
     */
    protected array|object|null $jsonData = NULL;
    
    /**
     * Empty constructor
     */
    public function __construct()
    {
    }
    
    /**
     * Inject primary dependencies - called by DIC in Server
     * 
     * @param Nette\Http\IRequest $request
     * @param Nette\Http\IResponse $response
     * @param Nette\Security\User $user
     * @param Nette\Http\Session $session
     * @param Query\Processor $processor
     * @param Nette\Application\LinkGenerator $linkGenerator
     */
    public function injectPrimary(
            Nette\Http\IRequest $request,
            Nette\Http\IResponse $response,
            Nette\Security\User $user,
            Nette\Http\Session $session,
            Query\Processor $processor,
            Nette\Application\LinkGenerator $linkGenerator
        ): void
    {
        if($this->primaryInjected) {
            return;
        }
        
        $this->user = $user;
        $this->request = $request;
        $this->response = $response;
        $this->session = $session;
        $this->processor = $processor;
        $this->linkGenerator = $linkGenerator;
       
        $this->primaryInjected = TRUE;
    }
    
    /**
     * Inject primary dependencies - called by Server
     * 
     * @param string $controllerName
     * @param string $serviceName
     * @param string $methodName
     * @param array $parameters
     * @param array|object $jsonData JSON data (REST API ONLY)
     */
    public function setParameters(
            string $controllerName,
            string $serviceName,
            string $methodName,
            array $parameters = [],
            array|object|null $jsonData = NULL,
        ): void
    {
        if($this->parametersSet) {
            return;
        }
        
        $this->controllerName = $controllerName;
        $this->serviceName = $serviceName;
        $this->methodName = $methodName;
        $this->parameters = $parameters;
        $this->jsonData = $jsonData;
        
        $this->parametersSet = true;
    }

    /**
     * Is Startup called?
     * 
     * @return bool
     */
    public function isStartupCalled(): bool
    {
        return $this->startupCalled;
    }

    /**
     * Startup method
     * 
     * @return void
     */
    public function startup(): void
    {
        $this->startupCalled = true;
    }
 
    
    /**
     * Terminate API controller correctly
     * 
     * @throws AbortException
     * @return never
     */
    public function terminate(): void
    {
        throw new AbortException();
    }
}