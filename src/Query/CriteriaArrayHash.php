<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Criteria ArrayHash
 */
class CriteriaArrayHash extends Nette\Utils\ArrayHash
{
    
	/** @return static */
	public static function from(array $arr, bool $recursive = true): static
	{
		$obj = new static;
		foreach ($arr as $key => $value) {
            if(!is_string($key)) {
                throw new Nette\InvalidArgumentException('Only strings are allowed as keys');
            }
            if(!($value instanceof Criteria)) {
                throw new Nette\InvalidArgumentException('Only instances of '.Criteria::class.' can be added.');
            }                
            $obj->$key = $value;
		}
		return $obj;
	}    
    
    public function offsetSet($key, $value): void
    {
        if(!($value instanceof Criteria)) {
            throw new Nette\InvalidArgumentException('Only instances of '.Criteria::class.' can be added.');
        }
        if(!is_string($key)) {
            throw new Nette\InvalidArgumentException('Only strings are allowed as keys');
        }
        parent::offsetSet($key, $value);
    }
    
}
