<?php

namespace AbraD450\ApiServer\Query;

use Nette;
use Nette\Utils\Strings;
use Nette\Database\Connection;
use Nette\Database\Row;

use AbraD450\ApiServer\Query\Query;
use AbraD450\ApiServer\Query\Criteria;
use AbraD450\ApiServer\Query\Processor;

/**
 * Raw SQL Query Processor
 */
class SqlQueryProcessor
{
    use Nette\SmartObject;
    
    /**
     * Default values
     * @var array
     */
    protected static array $defaults = [
        // output format of the \DateTimeInterface object
        'dateTimeFormat' => DATE_ISO8601,
        // output format of the \DateInterval object
        'timeFormat' => '%H:%M:%S',
        // Max page size
        'maxPageSize' => 500,
        // pagination for the collections (true = always, false = never, auto = when pageSize is present in the Query)
        'pagination' => 'auto'
    ];
    
    /**
     * Event OnRow
     * 
     * @var callable[]
     */
    public $onRow;

    /**
     * Event OnProcessedRow
     * 
     * @var callable[]
     */
    public $onProcessedRow;
    
    /**
     * Configuration
     * @var array
     */
    protected array $config;
    
    /**
     * Database connection
     * @var Connection
     */
    protected Connection $db;
    
    
    /**
     * Get current configuration
     * 
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
        
    /**
     * Configure
     * 
     * @param string $option
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    public function configure(string $option, mixed $value): void
    {
        if(!array_key_exists($option, self::$defaults)) {
            throw new \InvalidArgumentException("Parameter '{$option}' is not valid configuration option!");
        }
        $this->config[$option] = $value;
    }
    
    
    /**
     * Constructor
     * 
     * @param Connection $db
     * @param array $settings
     */
    public function __construct(Connection $db, array $settings = [])
    {
        $this->db = $db;
        if(is_array($settings)) {
            $this->config = Nette\Schema\Helpers::merge($settings, self::$defaults);
        }
        else {
            $this->config = self::$defaults;
        }
    }

    /**
     * Process SQL query
     * 
     * @param string $sql SQL Query
     * @param array $params Initial parameters for SQL query
     * @param Query $query
     * @param array $propColMap propertyFieldName => database_field_name
     * @param array $searchableCols Searchable columns array
     * @return array
     */
    public function process(string $sql, array $params, Query $query = NULL, array $propColMap = [], array $searchableCols = [], bool $skipLimitOffset = FALSE): array
    {
        $colPropMap = array_flip($propColMap);
        
        [$countSql, $countParams] = $this->prepareForCount($sql, $query, $propColMap, $searchableCols);
        [$dataSql, $dataParams] = $this->prepareForData($sql, $query, $propColMap, $searchableCols, $skipLimitOffset);
        
        $countParamsFinal = $params;
        foreach($countParams as $cPar) {
            $countParamsFinal[] = $cPar;
        }
        $total = $this->db->queryArgs($countSql, $countParamsFinal)->fetchField();
        
        $dataParamsFinal = $params;
        foreach($dataParams as $cPar) {
            $dataParamsFinal[] = $cPar;
        }
        $data = $this->db->queryArgs($dataSql, $dataParamsFinal);
        
        $limit = $offset = NULL;
        if(!$skipLimitOffset) {
            $criteria = $query ? $query->getCriteriaByPath() : NULL;
            [$limit, $offset] = $this->getLimitOffset($criteria);
        }

        $to = $skipLimitOffset ? NULL : min($offset + $limit - 1, $total - 1);
        
        $criteria = $query ? $query->getCriteriaByPath() : NULL;
        $pageSize = $criteria ? $criteria->getPageSize() : NULL;
        $page = $criteria ? $criteria->getPage() : NULL;
        $showPagination = (!empty($pageSize) || !empty($page) || $this->config['pagination'] === TRUE) && $this->config['pagination'] !== FALSE;        
        $outputData = [];
        if($showPagination) {
            $output = [
                'data' => &$outputData, 
                'total' => $total, 
                'page' => $skipLimitOffset ? NULL : $offset/$limit + 1,
                'pageSize' => $skipLimitOffset ? NULL : $limit, 
                'from' => $skipLimitOffset ? NULL : ($offset > $to ? null : $offset), 
                'to' => $skipLimitOffset ? NULL : ($offset > $to ? null : $to)
            ];
        }
        else {
            $output = &$outputData;
        }
        
        foreach($data as $item) {
            $item = $this->processRow($item, $colPropMap);
            if($item !== NULL) {
                $outputData[] = $item;
            }
        }
        
        return $output;
    }
    
    /**
     * Map columns to props
     * 
     * @param Row $row
     * @param array $propColMap
     */
    protected function processRow(Row $row, array $propColMap = [])
    {
        $eventData = new ProcessRowEventData($row, $propColMap);
        $this->onRow($eventData);
        if($eventData->shouldSkip) {
            return NULL;
        }
        
        $output = [];
        foreach($row as $col => $val) {
            $prop = $propColMap[$col] ?? $col;
            
            // Convert DateTime + Time values
            if($val instanceof \DateTimeInterface && !empty($this->config['dateTimeFormat'])) {
                $val = $val->format($this->config['dateTimeFormat']);
            }
            elseif($val instanceof \DateInterval && !empty($this->config['timeFormat'])) {
                $val = $val->format($this->config['timeFormat']);
            }
            else {
                $val = $val;
            }            
            
            $output[$prop] = $val;
        }

        $pEventData = new ProcessedRowEventData($row, $propColMap, $output);
        $this->onProcessedRow($pEventData);
        if($pEventData->shouldSkip) {
            return NULL;
        }        

        // Set other properties as defined in onProcessedRow callback
        if(!empty($pEventData->set)) {
            foreach($pEventData->set as $fieldName => $fieldValue) {
                $output[$fieldName] = $fieldValue;
            }
        }
        
        // Properties that must be removed as defined in onProcessedRow callback
        if(!empty($pEventData->unset)) {
            foreach($pEventData->unset as $fieldName) {
                unset($output[$fieldName]);
            }
        }        
        
        return $output;
    }
    
    /**
     * Process SQL query
     * 
     * @param string $sql SQL Query
     * @param Query $query
     * @param array $propColMap propertyFieldName => database_field_name
     * @param array $searchableCols Searchable database fields array
     * @return array
     */    
    protected function prepareForCount(string $sql, Query $query = NULL, array &$propColMap = [], array &$searchableCols = []): array
    {
        $params = [];
        
        $alias = '_view_';
        $wrappedSql = "SELECT COUNT(*) FROM ({$sql}) AS {$alias} WHERE 1=1";
        
        if($query === NULL) {
            return [$wrappedSql, $params];
        }
        
        $criteria = $query->getCriteriaByPath();
        if($criteria) {
            $this->applyCriteriaFilter($criteria, $wrappedSql, $params, $propColMap);  
            $this->applyCriteriaSearch($criteria, $wrappedSql, $params, $searchableCols);
        }
        return [$wrappedSql, $params];
    }    
    
    /**
     * Process SQL query
     * 
     * @param string $sql SQL Query
     * @param Query $query
     * @param array $propColMap propertyFieldName => database_field_name
     * @param array $searchableCols Searchable database fields array
     * @param boolean $skipLimitOffset Skip Limit Offset
     * @return array
     */    
    protected function prepareForData(string $sql, Query $query = NULL, array &$propColMap = [], array &$searchableCols = [], bool $skipLimitOffset = FALSE): array
    {
        $params = [];
        
        $alias = '_view_';
        
        if($query === NULL) {
            $wrappedSql = "SELECT * FROM ({$sql}) AS {$alias} WHERE 1=1";
            return [$wrappedSql, $params];
        }

        $selectStructure = $query->getSelectStructure();
        $select = $this->getSelect($selectStructure, $propColMap, $alias);
        
        $wrappedSql = "SELECT {$select} FROM ({$sql}) AS {$alias} WHERE 1=1";
        $criteria = $query->getCriteriaByPath();
        if($criteria) {
            $this->applyCriteriaFilter($criteria, $wrappedSql, $params, $propColMap);  
            $this->applyCriteriaSearch($criteria, $wrappedSql, $params, $searchableCols);
            $this->applyCriteriaSort($criteria, $wrappedSql, $propColMap);
            if(!$skipLimitOffset) {
                $this->applyCriteriaLimit($criteria, $wrappedSql);
            }
        }
        else {
            if(!$skipLimitOffset) {
                $this->applyDefaultLimit($wrappedSql);
            }
        }
        
        return [$wrappedSql, $params];
    }
    
    /**
     * Applies select command to a given selection
     * 
     * @param array $selectStructure
     * @param array $propColMap
     * @param string $prefix Prefix for column names (will be separated by DOT)
     * @return string
     */
    protected function getSelect(array $selectStructure, array &$propColMap, string $prefix = NULL): string
    {
        $realPrefix = $prefix ? "{$prefix}." : "";
        $sqlSelect = [];
        foreach($selectStructure as $prop => $val) {
            
            // Select all
            if($prop === '*') {        
                $sqlSelect = [];
                break;
            }
            
            // Property to column
            $col = $propColMap[$prop] ?? $prop;
            
            if(is_bool($val)) {
               $sqlSelect[$realPrefix.$col] = TRUE;
               continue;
            }
        }
        return count($sqlSelect) <= 0 ? "{$realPrefix}*" : implode(',', array_keys($sqlSelect));
    }    
    
    /**
     * Get Limit + Offset from Criteria
     * @param Criteria $criteria
     * @return array
     */
    protected function getLimitOffset(Criteria $criteria = NULL): array
    {
        if($criteria) {
            $pageSize = $criteria->getPageSize();
            $page = $criteria->getPage();
            if(isset($pageSize) || isset($page)) {
                $page = $page ?? 1;
                $pageSize = $pageSize ?? $this->config['maxPageSize'];
                $realPageSize = min($this->config['maxPageSize'], $pageSize);
                $offset = isset($page) ? ($page-1) * $realPageSize : 0;
                return [$realPageSize, $offset];
            }
        }
        return [$this->config['maxPageSize'], 0];
    }
    
    /**
     * Apply default limit/offset (limit = configured maxPageSize + offset = 0
     * 
     * @param string $sql
     * @return void
     */
    protected function applyDefaultLimit(string &$sql): void
    {
        $offset = 0;
        $limit = $this->config['maxPageSize'];
        $sql .= " LIMIT {$limit} OFFSET {$offset}";
    }
    
    /**
     * Apply limit/offset
     * 
     * @param Criteria $criteria
     * @param string $sql
     */
    protected function applyCriteriaLimit(Criteria $criteria, string &$sql): void
    {
        [$limit, $offset] = $this->getLimitOffset($criteria);
        $sql .= " LIMIT {$limit} OFFSET {$offset}";
    }
    
    /**
     * Apply sorting
     * 
     * @param Criteria $criteria
     * @param select $sql
     * @param array $propColMap
     */
    protected function applyCriteriaSort(Criteria $criteria, string &$sql, array &$propColMap): void
    {
        $sortList = $criteria->getSort();
        if(!$sortList) {
            return;
        }
        $orders = [];
        foreach($sortList as $sort) {
            /* @var $sort Sort */
            $col = $propColMap[$sort->field] ?? $sort->field;
            $dir = strtoupper($sort->dir) === "DESC" ? "DESC" : "ASC";
            $orders[] = "{$col} {$dir}";
        }
        if(!empty($orders)) {
            $sql .= " ORDER BY ".implode(', ', $orders);
        }
    }
    
    /**
     * Apply search
     * 
     * @param Criteria $criteria
     * @param string $sql SQL query
     * @param array $params Parameters for the SQL query
     * @param array $searchableCols
     */
    protected function applyCriteriaSearch(Criteria $criteria, string &$sql, array &$params, array &$searchableCols): void
    {
        $search = $criteria->getSearch();
        
        if(!isset($search) || trim($search) === '') {
            return;
        }

        // Apply search to all searchable columns
        $searchablesCount = count($searchableCols);
        if($searchablesCount <= 0) {
            return;
        }
        
        $sql .= " AND (LOWER(".implode(') LIKE ? OR LOWER(', $searchableCols).') LIKE ?)';
        for($i = 0; $i < $searchablesCount; $i++) {
            $params[] = '%'.Strings::lower($search).'%';
        }
    }
    
    /**
     * Apply filtering 
     * 
     * @param Criteria $criteria
     * @param string $sql
     * @param array $params
     * @param array $propColMap
     */
    protected function applyCriteriaFilter(Criteria $criteria, string &$sql, array &$params, array &$propColMap)
    {
        $filterGroup = $criteria->getFilter();
        if(!$filterGroup) {
            return;
        }
        
        $filtersParams = [];
        $filtersSql = $this->applyCriteriaFilterGroup($filterGroup, $propColMap, $filtersParams);
        if($filtersSql !== NULL) {
            $sql .= " AND {$filtersSql}";
            foreach($filtersParams as $p) {
                $params[] = $p;
            }
        }
    }    
    
    
    /**
     * Recursive function for applying filters from given filter group
     * 
     * @param FilterGroup $filterGroup
     * @param \stdClass $meta
     * @param array $params SQL params (output parameter)
     * @return string|null
     */
    protected function applyCriteriaFilterGroup(FilterGroup $filterGroup, &$propColMap, &$params): ?string
    {
        $sqlArray = [];
        foreach($filterGroup->getFilters() as $f) {
            if($f instanceof FilterGroup) {
                /* @var $f FilterGroup */
                $groupResult = $this->applyCriteriaFilterGroup($f, $propColMap, $params);
                if($groupResult !== NULL) {
                    $sqlArray[] = $groupResult;
                }
                continue;
            }
            $field = $f->getField();
            $col = $propColMap[$field] ?? $field;
            /* @var $f Filter */
            $sql = Processor::generateFilter($col, $f->getOperator(), $f->getValue(), $params);
            $sqlArray[] = $sql;
        }
        return count($sqlArray) > 0 ? '('.implode(' '.strtoupper($filterGroup->getLogic()).' ', $sqlArray).')' : NULL;
    }    
   


    /**
     * get defaults
     * @return array
     */
    public static function getDefaults(): array
    {
        return self::$defaults;
    }
    
}
