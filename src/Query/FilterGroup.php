<?php

namespace AbraD450\ApiServer\Query;

use Nette;
use Nette\Utils\ArrayList;

/**
 * Filter Group
 * 
 * @property string $logic
 * @property Filter[]|FilterGroup[] $filters
 */
class FilterGroup
{
    use Nette\SmartObject;
 
    const LOGIC_AND = 'and';
    const LOGIC_OR = 'or';
    
    /**
     * Logic
     * 
     * @var string
     */
    protected $logic;

    /**
     * Filters
     * 
     * @var FilterArrayList
     */
    protected $filters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->filters = new FilterArrayList();
        $this->logic = self::LOGIC_AND;
    }
    
    
    public function getLogic(): string
    {
        return $this->logic;
    }

    public function getFilters(): FilterArrayList
    {
        return $this->filters;
    }

    public function setLogic(string $logic)
    {
        $this->logic = strtolower($logic) === self::LOGIC_OR ? self::LOGIC_OR : self::LOGIC_AND;
        return $this;
    }

    public function setFilters(FilterArrayList $filters)
    {
        $this->filters = $filters;
        return $this;
    }
    
    
    /**
     * Create FilterGroup structure from given data
     * 
     * @param array|object $data
     * @return self
     */
    public static function create($data)
    {
        $group = new static();
        foreach($data as $prop => $value) {
            switch(strtolower($prop)) {
                case 'logic':
                    $group->setLogic($value);
                    break;
                case 'filters':
                    if(count($value) === 0) {
                        break;
                    }
                    
                    // if this is not array of filters, but the filter itself, create an array with one array item inside
                    if(!isset($value[0])) {
                        $value = [$value];
                    }
                        
                    foreach($value as $fVal) {
                        if((is_array($fVal) && isset($fVal['logic'])) || (is_object($fVal) && isset($fVal->logic))) {
                            $group->filters[] = FilterGroup::create($fVal);
                        }
                        else {
                            $group->filters[] = Filter::create($fVal);
                        }
                    }
                    break;
            }
        }
        return $group;
    }    
}