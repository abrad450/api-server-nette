<?php

namespace AbraD450\ApiServer\Query;

use Nette;
use Nette\Utils\Strings;

use AbraD450\MappedDatabase\Entity\Entity;
use AbraD450\MappedDatabase\Table\ISelection;
use AbraD450\MappedDatabase\Table\Selection;
use AbraD450\MappedDatabase\Table\GroupedSelection;
use AbraD450\MappedDatabase\Entity\EntityMetaStorage;

/**
 * Processor
 * 
 * @property-read array $config Configuration
 */
class Processor
{
    use Nette\SmartObject;
    
    /**
     * Event before entity is processed
     * If callback returns false entity will not be sent to the output
     * 
     * @var callable[]
     */
    public $onEntity;

    /**
     * Event after entity is processed
     * Can be used to add / remove properties
     * 
     * @var callable[]
     */
    public $onProcessedEntity;

    /**
     * Event before selection is executed
     * Can for example change / add some ->where statements for access control
     * If callback returns false selection will not be sent to the output
     * 
     * @var callable[]
     */
    public $onSelection;

    /**
     * Event called before the search on selection is being applied
     * Listeners can customize columns used for searching
     * Callback signature is (object $searchEvent) that has properties 'searchables', 'table', 'entity'
     * 
     * @var callable[]
     */
    public $onSearch;
    
    /**
     * Hashmap of CriteriaAdapters [type => instance]
     */
    protected array $adapters = [];
    
    /**
     * Defaults 
     * 
     * @var array
     */
    protected static $defaults = [
        // output format of the \DateTimeInterface object for datetime value
        'dateTimeFormat' => DATE_ATOM,
        // output format of the \DateTimeInterface object for date value
        'dateFormat' => 'Y-m-d',
        // output format of the \DateTimeInterface object for time value
        'timeFormat' => 'H:i:s',
        // output format of the \DateInterval object for time value
        'timeIntervalFormat' => '%H:%M:%S',
        // maximum number of records sent to the output for collections
        'maxPageSize' => 100,
        // pagination for the collections (true = always, false = never, auto = when pageSize is present in the Query)
        'pagination' => 'auto'
    ];
    
    /**
     * config
     * 
     * @var array 
     */
    protected $config;
    
    /**
     * Get current configuration
     * 
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
    
    /**
     * Configure
     * 
     * @param string $option
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    public function configure(string $option, mixed $value): void
    {
        if(!array_key_exists($option, self::$defaults)) {
            throw new \InvalidArgumentException("Parameter '{$option}' is not valid configuration option!");
        }
        $this->config[$option] = $value;
    }
        
    /**
     * Constructor
     * 
     * @param array $settings
     */
    public function __construct(?array $settings = NULL)
    {
        if(is_array($settings)) {
            $this->config = Nette\Schema\Helpers::merge($settings, self::$defaults);
        }
        else {
            $this->config = self::$defaults;
        }
    }
        
    /**
     * Process Entity or Selection
     * 
     * @param Entity|Selection $what
     * @param Query $query
     * @param bool $strict (Construct keys only for entities)
     * @param bool $skipLimitOffset
     * @return array
     */
    public function process(Entity|Selection|null $what, Query $query = NULL, bool $strict = FALSE, bool $skipLimitOffset = FALSE)
    {        
        $qry = $query ?? new Query();
        
        if($what instanceof Entity) {
            return $this->processEntity($what, $qry, $strict);
        }
        
        if($what instanceof Selection) {
            return $this->processSelection($what, $qry, $strict, skipLimitOffset: $skipLimitOffset);
        }
        
        if($what === NULL) {
            return NULL;
        }
        
        throw new Nette\InvalidArgumentException('Argument #0 must be either Entity or Selection');
    }
    
    /**
     * Process Entity
     * 
     * @param Entity $entity
     * @param Query $query
     * @param bool $strict (Construct keys only for entities)
     * @return array
     */
    public function processEntity(Entity $entity, Query $query = NULL, bool $strict = FALSE)
    {
        $qry = $query ?? new Query();
        return $this->processingEntity($entity, $qry, $qry->getSelectStructure(), [], $strict);
    }
    
    /**
     * Process Selection
     * 
     * @param Selection $selection
     * @param Query $query
     * @param bool $strict (Construct keys only for entities)
     * @param bool $skipLimitOffset
     * @return array
     */
    public function processSelection(ISelection $selection, Query $query = NULL, bool $strict = FALSE, bool $skipLimitOffset = FALSE)
    {
        $qry = $query ?? new Query();
        return $this->processingSelection($selection, $qry, $qry->getSelectStructure(), [], $strict, skipLimitOffset: $skipLimitOffset);
    }
    
    
    
    
    /**
     * Processing an Entity
     * 
     * @param Entity $entity
     * @param Query $query
     * @param array $selectStructure What to select
     * @param array $path Where are we in the Query
     * @param bool $constructKeysOnly If * is selected, use only construct keys from DB (might be selected on Selection)
     * @return array
     */
    protected function processingEntity(Entity $entity, Query $query, array $selectStructure, array $path = [], $constructKeysOnly = FALSE)
    {
        $queryAdapter = $this->getQueryAdapter($entity->getTable());
        $formats = [
            'date' => $this->config['dateFormat'],
            'datetime' => $this->config['dateTimeFormat'],
            'time' => $this->config['timeFormat'],
            'timeinterval' => $this->config['timeIntervalFormat']
        ];
        
        $eventData = new ProcessEntityEventData($entity, $selectStructure);
        $this->onEntity($eventData);
        if($eventData->shouldSkip) {
            return NULL;
        }
        
        $meta = $entity->getEntityMetadata();

        $output = [];
       
        // Select all props
        if(isset($selectStructure['*'])) {
            // Select all available fields
            $output = $entity->toArray(function($val, $p) use(&$meta, $queryAdapter, $formats) {
                $pMeta = $meta->properties[$p] ?? null;
                return $queryAdapter->valueForOutput($val, $pMeta->type ?? null, $pMeta->nativeType ?? null, $formats);
            }, $constructKeysOnly, true, true);
        }
        
        // Select some props (must be combined with * !)
        foreach($selectStructure as $prop => $selectSubStructure) {
                        
            // Property is not defined OR its access is write-only - skip
            if(!isset($meta->properties[$prop]) || $meta->properties[$prop]->access === 'w') {
                continue;
            }
            // OR property is already filled (*) or is *
            if(array_key_exists($prop, $output) || $prop === '*') {
                continue;
            }
            
            // Property is defined on entity AND its access is not write only (passwords)
            
            // if we request ref / related property AND with TRUE (so no subselect was defined)
            // ... create default subselect ( id,userRoles ) => ( id,userRoles(*) )
            //dump($prop, $meta->properties[$prop]);
            if(is_bool($selectSubStructure) && ($meta->properties[$prop]->ref || $meta->properties[$prop]->related)) {
                $selectSubStructure = ['*' => TRUE];
            }

            // Current level
            if(is_bool($selectSubStructure)) {
                
                $val = $entity->offsetGet($prop);
                
                $pMeta = $meta->properties[$prop];
                $output[$prop] = $queryAdapter->valueForOutput($val, $pMeta->type, $pMeta->nativeType, $formats);
                continue;
            }

            // SubStructure - next level
            $refRelated = $entity->offsetGet($prop);
                        
            if($refRelated instanceof Entity) {
                $newPath = $path;
                $newPath[] = $prop;
                $refProcessed = $this->processingEntity($refRelated, $query, $selectSubStructure, $newPath, $constructKeysOnly);
                if($refProcessed !== NULL) {
                    $output[$prop] = $refProcessed;
                }
            }
            elseif($refRelated instanceof Selection || $refRelated instanceof GroupedSelection) {
                
                // condition for collection - if defined
                if(isset($meta->properties[$prop]->condition)) {
                    $refRelated->where($meta->properties[$prop]->condition);
                }
                
                $newPath = $path;
                $newPath[] = $prop;
                $relatedProcessed = $this->processingSelection($refRelated, $query, $selectSubStructure, $newPath);
                if($relatedProcessed !== NULL) {
                    $output[$prop] = $relatedProcessed;
                }
            }
            else {
                $output[$prop] = $refRelated;
            }

        }
        
        $pEventData = new ProcessedEntityEventData($entity, $selectStructure, $output);
        $this->onProcessedEntity($pEventData);
        if($pEventData->shouldSkip) {
            return NULL;
        }

        // Set other properties as defined in onEntity callback
        if(!empty($pEventData->set)) {
            foreach($pEventData->set as $fieldName => $fieldValue) {
                $output[$fieldName] = $fieldValue;
            }
        }
        
        // Properties that must be removed as defined in onEntity callback
        if(!empty($pEventData->unset)) {
            foreach($pEventData->unset as $fieldName) {
                unset($output[$fieldName]);
            }
        }
        
        return $output;
    }
    
    
    /**
     * Processing Selection / GroupedSelection
     * 
     * @param ISelection $selection
     * @param Query $query
     * @param array $selectStructure What to select in this level
     * @param array $path Where are we in the Query
     * @param bool $constructKeysOnly If * is selected, use only construct keys from DB (might be selected on Selection)
     * @param bool $skipLimitOffset 
     * @return array
     */
    protected function processingSelection(ISelection $selection, Query $query, array $selectStructure, array $path = [], $constructKeysOnly = FALSE, bool $skipLimitOffset = FALSE)
    {
        $queryAdapter = $this->getQueryAdapter($selection);
        
        $meta = EntityMetaStorage::describe($selection->getEntityName());

        $criteria = $query->getCriteriaByPath($path);
        if($criteria) {
            $this->applyCriteriaSort($criteria, $selection, $meta, $queryAdapter);
            $this->applyCriteriaSearch($criteria, $selection, $meta, $queryAdapter);
            $this->applyCriteriaFilter($criteria, $selection, $meta, $queryAdapter);            
            if(!$skipLimitOffset) {
                $this->applyCriteriaLimit($criteria, $selection, $queryAdapter);
            }
        }
        else {
            if(!$skipLimitOffset) {
                $this->applyDefaultLimit($selection, $queryAdapter, $queryAdapter);
            }
        }
        $this->applySelect($selectStructure, $selection, $meta, $queryAdapter);
        
        $eventData = new ProcessSelectionEventData($selection, $selectStructure);
        $this->onSelection($eventData);
        if($eventData->shouldSkip) {
            return NULL;
        }

        $pageSize = $criteria->pageSize ?? null;
        $page = $criteria->page ?? null;
        $showPagination = !$skipLimitOffset &&
                (!empty($pageSize) || !empty($page) || $this->config['pagination'] === TRUE) &&
                $this->config['pagination'] !== FALSE;

        $outputData = [];
        if($showPagination) {
            $backupLimit = $selection->getSqlBuilder()->getLimit();
            $backupOffset = $selection->getSqlBuilder()->getOffset();
//            $selection->limit(NULL, NULL);
            $total = $selection->count('*');
//            $selection->limit($backupLimit, $backupOffset);
            $to = min($backupOffset + $backupLimit - 1, $total - 1);
            $output = [
                'data' => &$outputData, 
                'total' => $total, 
                'page' => $skipLimitOffset ? NULL : ($backupOffset/$backupLimit+1), 
                'pageSize' => $skipLimitOffset ? NULL : ($backupLimit), 
                'from' => $skipLimitOffset ? NULL : ($backupOffset > $to ? null : $backupOffset),
                'to' => $skipLimitOffset ? NULL : ($backupOffset > $to ? null : $to)
            ];
        }
        else {
            $output = &$outputData;
        }
        
        foreach($selection as $entity) {
            $entity = $this->processingEntity($entity, $query, $selectStructure, $path, $meta, $constructKeysOnly);
            if($entity !== NULL) {
                $outputData[] = $entity;
            }
        }
        return $output;
    }
    

    /**
     * Applies select command to a given selection
     * 
     * @param array $selectStructure Array of columns to be selected
     * @param ISelection $selection
     */
    protected function applySelect(array $selectStructure, ISelection $selection, $meta)
    {
        $sqlSelect = [];
        foreach($selectStructure as $field => $val) {
            
            // Select all
            if($field === '*') {
                // fill all properties manually
                foreach($meta->properties as $pm) {
                    if(empty($pm->ref) && empty($pm->related) && empty($pm->virtual)) {
                        $sqlSelect[$pm->col] = TRUE;
                    }
                }
                continue;
            }
            
            // Property does not exist - skip it
            if(!isset($meta->properties[$field])) {
                continue;
            }
            
            $propMeta = $meta->properties[$field];
            
            // we must skip virtual, these are not real columns
            if(!empty($propMeta->virtual)) {
                continue;
            }
            
            // It is not bool => it is ref/related
            // If it is ref property, ensure that "through" column is also selected
            // We wouldn't be able to process the referenced object otherwise
            if(!empty($propMeta->ref)) {
                $sqlSelect[$propMeta->through] = TRUE;
                continue;
            }
            
            // we must skip related, these are not columns
            if(!empty($propMeta->related)) {
                continue;
            }
            
            if(is_bool($val)) {
               $sqlSelect[$propMeta->col] = TRUE;
               continue;
            }
             
        }
        
        // Prefix all columns with table name
        $sName = $selection->getName();
        $new = [];
        foreach($sqlSelect as $key => $val) {
            $new[$sName.'.'.$key] = $val;
        }
        
        $selection->select(count($sqlSelect) === 0 ? $sName.'.*' : implode(',', array_keys($new)));
    }
    
    /**
     * Apply default limit/offset
     * 
     * @param ISelection $selection
     */
    protected function applyDefaultLimit(ISelection $selection, Adapters\QueryAdapter $queryAdapter)
    {
        $queryAdapter->applyLimitOffset($selection, $this->config['maxPageSize'], 0);
    }
    
    /**
     * Apply limit/offset
     * 
     * @param Criteria $criteria
     * @param Selection $selection
     */
    protected function applyCriteriaLimit(Criteria $criteria, ISelection $selection, Adapters\QueryAdapter $queryAdapter)
    {
        $pageSize = $criteria->getPageSize();
        $page = $criteria->getPage();
        if(isset($pageSize) || isset($page)) {
            $page = $page ?? 1;
            $pageSize = $pageSize ?? $this->config['maxPageSize'];
            $realPageSize = min($this->config['maxPageSize'], $pageSize);
            $offset = isset($page) ? ($page-1) * $realPageSize : 0;
            
            $queryAdapter->applyLimitOffset($selection, $realPageSize, $offset);
        }
        else {           
            $this->applyDefaultLimit($selection, $queryAdapter);
        }
    }
    
    /**
     * Apply sorting
     * 
     * @param Criteria $criteria
     * @param Selection $selection
     * @param \stdClass $meta
     */
    protected function applyCriteriaSort(Criteria $criteria, ISelection $selection, $meta, Adapters\QueryAdapter $queryAdapter)
    {
        $sortList = $criteria->getSort();
        if(!$sortList) {
            return;
        }
        foreach($sortList as $sort) {
            /* @var $sort Sort */
            $colPath = $this->fieldPathToDbPath($sort->field, $meta);
            if(!$colPath) {
                continue;
            }
            $dir = strtoupper($sort->dir);
            
            $queryAdapter->applyOrder($selection, $colPath, $dir);
        }
    }
    
    /**
     * Apply search
     * 
     * @param Criteria $criteria
     * @param Selection $selection
     * @param \stdClass $meta
     */
    protected function applyCriteriaSearch(Criteria $criteria, ISelection $selection, $meta, Adapters\QueryAdapter $queryAdapter)
    {
        $search = $criteria->getSearch();
        
        // is null or empty
        if(!isset($search) || trim($search) === '') {
            return;
        }
        
        // On search - possibility to customize columns where to search
        $event = new \stdClass();
        $event->searchables = array_values($meta->searchables);
        $event->table = $meta->table;
        $event->entity = $meta->name;
        $this->onSearch($event);

        // Apply search to all searchable columns
        $searchablesCount = count($event->searchables);
        if($searchablesCount <= 0) {
            return;
        }
        
        $queryAdapter->applySearch($selection, $event->searchables, $search);                
    }
    
    /**
     * Apply filtering 
     * 
     * @param Criteria $criteria
     * @param Selection $selection
     * @param \stdClass $meta
     */
    protected function applyCriteriaFilter(Criteria $criteria, ISelection $selection, $meta, Adapters\QueryAdapter $queryAdapter)
    {
        $filterGroup = $criteria->getFilter();
        if(!$filterGroup) {
            return;
        }
        $params = [];
        $sql = $this->applyCriteriaFilterGroup($criteria->filter, $meta, $params, $queryAdapter);
        if($sql !== NULL) {
            $queryAdapter->applyWhere($selection, $sql, $params);                    
        }
    }
    
    /**
     * Recursive function for applying filters from given filter group
     * 
     * @param FilterGroup $filterGroup
     * @param \stdClass $meta
     * @param array $params SQL params (output parameter)
     * @return string|null
     */
    protected function applyCriteriaFilterGroup(FilterGroup $filterGroup, $meta, &$params, Adapters\QueryAdapter $queryAdapter): ?string
    {
        $sqlArray = [];
        foreach($filterGroup->getFilters() as $f) {
            if($f instanceof FilterGroup) {
                /* @var $f FilterGroup */
                $groupResult = $this->applyCriteriaFilterGroup($f, $meta, $params, $queryAdapter);
                if($groupResult !== NULL) {
                    $sqlArray[] = $groupResult;
                }
                continue;
            }
            $field = $f->getField();
            $finalMeta = null;
            $colPath = $this->fieldPathToDbPath($field, $meta, $finalMeta);
            
            // Property path does not exist - silently skip this rule
            if(!$colPath) {
                continue;
            }
            // Required ref/related/virtual
            if(!empty($finalMeta->ref) || !empty($finalMeta->related) || !empty($finalMeta->virtual)) {
                continue;
            }
                        
            /* @var $f Filter */
            $sql = $this->generateFilter($colPath, $f->getOperator(), $f->getValue(), $params, $finalMeta, $queryAdapter);
            $sqlArray[] = $sql;
        }
        return count($sqlArray) > 0 ? '('.implode(' '.strtoupper($filterGroup->getLogic()).' ', $sqlArray).')' : NULL;
    }

    
    
    /**
     * Convert someReference.otherReference.fieldName to some_reference.other_reference.field_name 
     * according to metadata of particular entities
     * 
     * @param string $path
     * @param \stdClass $meta Input meta
     * @param \stdClass $finalMeta Final field meta
     * @return string|null
     */
    protected function fieldPathToDbPath(string $path, $meta, &$finalMeta = null): ?string
    {
        $pathParts = explode('.', $path);
        $initialPathPartsCount = count($pathParts);
        $dbPath = [];
        $m = $meta;
        $cMeta = null;
        //$isFirst = true;
        while(count($pathParts) > 0) {
            $partRaw = array_shift($pathParts);
            $part = ltrim($partRaw, ':');
            if(!isset($m->properties[$part])) {
                return NULL;
            }
            
            $cMeta = $m->properties[$part];
            
            if(!empty($cMeta->related)) {
                $dbPath[] = ':'.$cMeta->related; // table name with :
            }
            else {
                $dbPath[] =
                    //($isFirst ? $m->table.'.' : ''). // table name on first part (to prevent ambig. names)
                    ($initialPathPartsCount === 1 ? $m->table.'.' : '').
                    ($cMeta->through ?? $cMeta->col); // through (rel) or column name
            }
            
            // we still have some parts in array, but current part is not reference OR related => field not found
            if(count($pathParts) > 0) {
                if(empty($cMeta->ref) && empty($cMeta->related)) {
                    return NULL;
                }
                // switch to next meta
                $m = EntityMetaStorage::describe($cMeta->type);
            }
            //$isFirst = false;
        }
        $finalMeta = $cMeta;
        return implode('.', $dbPath);
    }
    
    
    /**
     * Get Query Adapter based on the given Selection
     * 
     * @param Selection|GroupedSelection $selection
     * @return Adapters\QueryAdapter
     */
    public function getQueryAdapter(Selection|GroupedSelection $selection): Adapters\QueryAdapter
    {
        $dsnGetter = \Closure::bind(
                fn(Selection|GroupedSelection $selection) => strtolower(explode(':', $selection->explorer->getConnection()->getDsn())[0]),
                null,
                $selection
            );
        $driverName = ucfirst($dsnGetter($selection));
        $adapterClass = __NAMESPACE__.'\\Adapters\\'.ucfirst($driverName).'QueryAdapter';
        if(!class_exists($adapterClass)) {
            $adapterClass =__NAMESPACE__.'\\Adapters\\GenericQueryAdapter';
        }

        if(!empty($this->adapters[$driverName])) {
            return $this->adapters[$driverName];
        }

        $this->adapters[$driverName] = new $adapterClass();
        return $this->adapters[$driverName];
    }
    
    /**
     * Generate filter, based on column, operator and value
     * adds parameters to $params
     * 
     * @param string $col
     * @param string $operator
     * @param mixed $value
     * @param array $params
     * @param object $fieldMeta Meta for the $col field
     * @param Adapters\QueryAdapter $queryAdapter Query Adapter
     * @return string
     */
    public function generateFilter(string $col, string $operator, mixed $value, array &$params, object $fieldMeta, Adapters\QueryAdapter $queryAdapter)
    {
        // column <> NULL => column IS NOT NULL
        if($operator === '<>' && $value === NULL) {
            $operator = 'is not null';
        }
        // column = NULL => column IS NOT NULL
        if($operator === '=' && $value === NULL) {
            $operator = 'is null';
        }
        
        $sql = '';
        switch($operator) {
            case 'startswith':
                $sql .= 'LOWER('.$col.') ';
                $sql .= 'LIKE ?';
                $params[] = Strings::lower($value).'%';
                break;
            case 'endswith':
                $sql .= 'LOWER('.$col.') ';
                $sql .= 'LIKE ?';
                $params[] = '%'.Strings::lower($value);
                break;
            case 'contains':
                $sql .= 'LOWER('.$col.') ';
                $sql .= 'LIKE ?';
                $params[] = '%'.Strings::lower($value).'%';
                break;
            case 'notcontains':
                $sql .= 'LOWER('.$col.') ';
                $sql .= 'NOT LIKE ?';
                $params[] = '%'.Strings::lower($value).'%';
                break;
            case 'is null':
            case 'is not null':
                $sql .= $col.' ';
                $sql .= strtoupper($operator);
                break;
            case 'in':
            case 'not in':
                $sql .= $col.' ';
                $sql .= strtoupper($operator).'(?)';
                $params[] = is_array($value) ? $value : [$value];
                break;

            // je cislo tydne?
            case 'isweek':
                $val = $queryAdapter->parseDateTimeValue($value);
                if($val) {
                    $sql .= '(CAST(EXTRACT(WEEK FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ? AND CAST(EXTRACT(YEAR FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ?)';
                    $params[] = $val->format("W");
                    $params[] = $val->format("Y");
                }
                elseif((int)$value <= 53 && (int)$value > 0) {
                    $sql .= 'CAST(EXTRACT(WEEK FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ?';
                    $params[] = (int)$value;
                }
                break;

            // je tento tyden?
            case 'thisweek':
                $now = new \DateTime();
                $sql .= '(CAST(EXTRACT(WEEK FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ? AND CAST(EXTRACT(YEAR FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ?)';
                $params[] = $now->format("W");
                $params[] = $now->format("Y");
                break;
				
            // je dnešek?
            case 'today':
                $now = new \DateTime();
                $sql .= '(CAST(EXTRACT(DAY FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ? AND CAST(EXTRACT(MONTH FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ? AND CAST(EXTRACT(YEAR FROM CAST('.$col.' AS DATE)) AS NUMERIC) = ?)';
                $params[] = $now->format("d");
                $params[] = $now->format("m");
                $params[] = $now->format("Y");
                break;

            // next operators like = < > <> >= <=
            default:
                $sql .= "{$col} {$operator} ?";
                $params[] = $queryAdapter->valueForFilter($value, $fieldMeta->type, $fieldMeta->nativeType);
                break;
        }
        return $sql;
    }

    
    
    /**
     * get defaults
     * @return array
     */
    public static function getDefaults(): array
    {
        return self::$defaults;
    }
    
}