<?php

namespace AbraD450\ApiServer\Query;

use AbraD450\MappedDatabase\Table\ISelection;

use Nette;

/**
 * Processor Event Data
 * 
 * @property bool $shouldSkip Should skip the whole selection
 * @property-read ISelection $selection
 * @property-read array $selectStructure
 */
class ProcessSelectionEventData extends ProcessorEventData
{
    use Nette\SmartObject;
    
    private bool $shouldSkip = FALSE;
    
    private ISelection $selection;
    
    private array $selectStructure;
    
    public function __construct(ISelection $selection, array &$selectStructure)
    {
        $this->selection = $selection;
        $this->selectStructure = &$selectStructure;
    }
    
    public function getSelection(): ISelection
    {
        return $this->selection;
    }
    
    public function &getSelectStructure(): array
    {
        return $this->selectStructure;
    }
    
    public function getShouldSkip(): bool
    {
        return $this->shouldSkip;
    }
    
    public function setShouldSkip(bool $shouldSkip)
    {
        $this->shouldSkip = $shouldSkip;
    }
}