<?php

namespace AbraD450\ApiServer\Query;

use Nette;
use Nette\InvalidArgumentException;

use AbraD450\ApiServer;


/**
 * API Query
 * 
 * @property string $select Select structure
 * @property-read array $selectStructure Select structure
 * @property CriteriaArrayHash $criteria Criteria
 */
class Query implements ApiServer\IApiConstructible
{
    use Nette\SmartObject;
    
    /**
     * Select structure
     * 
     * @var string
     */
    protected $select;
    
    /**
     * Select structure
     * 
     * @var array
     */
    protected $selectStructure;
    
    /**
     * Criteria
     * 
     * @var CriteriaArrayHash
     */
    protected $criteria;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->criteria = new CriteriaArrayHash();
        $this->setSelect('*');
    }
    
    
    public function getSelect(): string
    {
        return $this->select;
    }

    public function getCriteria(): CriteriaArrayHash
    {
        return $this->criteria;
    }

    public function setSelect(string $select)
    {
        $this->select = $select;
        $this->selectStructure = NULL;
        return $this;
    }
    
    public function setCriteria(CriteriaArrayHash $criteria)
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function getSelectStructure(): array
    {
        if($this->selectStructure === NULL) {
            $this->parseSelectStructure();
        }
        return $this->selectStructure;
    }
    
    
    /**
     * Returns criteria from Query identified by $path
     * 
     * @param array $path
     * @return Criteria
     */
    public function getCriteriaByPath(array $path = [])
    {
        $strPath = empty($path) ? '@' : implode('/', $path);
        return $this->criteria[$strPath] ?? NULL;
    }
    
//    /**
//     * Returns select array from Query identified by $path
//     * 
//     * @param array $path
//     * @return string[]
//     */
//    protected function getSqlSelectByPath(array $path = [])
//    {
//        $structure = $this->getSelectStructure();
//        foreach($path as $part) {
//            if(!isset($structure[$part])) {
//                return NULL;
//            }
//            $structure = $structure[$part];
//        }
//        return array_filter($structure, function($val) {
//            return is_bool($val);
//        });
//    }    
    
    
    
    /**
     * Create Query structure from given data
     * 
     * @param array|object $data (select, criteria, search, page, pageSize, sort, filter)
     * @return self
     */
    public static function create($data)
    {
        $isArray = is_array($data);
        $isObject = is_object($data);
        if(!$isObject && !$isArray) {
            throw new InvalidArgumentException('Parameter $data must be either an array or an object.');
        }
        $query = new static();
        foreach($data as $prop => $value) {
            switch(strtolower($prop)) {
                case 'select':
                    $query->setSelect($value);
                    break;
                case 'criteria':
                    foreach($value as $crKey => $crValue) {
                        $query->criteria[$crKey] = Criteria::create($crValue);
                    }
                    break;
            }
        }
        // Criteria props shortcut
        if(
            ($isArray && (isset($data['search']) || isset($data['page']) || isset($data['pageSize']) || isset($data['sort']) || isset($data['filter'])))
                || 
            ($isObject && (isset($data->search) || isset($data->page) || isset($data->pageSize) || isset($data->sort) || isset($data->filter)))) {
            
            $query->criteria['@'] = Criteria::create($data);
        
        }
            
        return $query;
    }
    
    
    
    /**
     * Parse select struct
     * 
     * id,url,name,createdStamp,createdUserId,createdUser(id,email,password,firstName,lastName,active,locked,posts(id,parentId,pos,name,slug,perex)),deleted
     * 
     * [
     *    'id' => true,
     *    'url' => true,
     *    'name' => true,
     *    'createdStamp' => true,
     *    'createdUserId' => true,
     *    'createdUser' => [
     *       'id' => true,
     *       'email' => true,
     *       'password' => true,
     *       'firstName' => true,
     *       'lastName' => true,
     *       'active' => true,
     *       'locked' => true,
     *       'posts' => [
     *          'id' => true,
     *          'parentId' => true,
     *          'pos' => true,
     *          'name' => true,
     *          'slug' => true,
     *          'perex' => true
     *       ]
     *    ],
     *    'deleted' => true
     * ]
     * 
     * 
     * 
     * // itemId,comodity,order(number,orderId,route(routeId,name)),consignee(subjectId,name,country(*))
     * // 
     * // [
     * //      '@' => ['itemId','comodity'],
     * //      'order' => [
     * //          '@' => ['number','orderId'],
     * //          'route' => [
     * //              '@' => ['routeId','name']
     * //          ]
     * //      ],
     * //      'consignee' => [
     * //          '@' => ['subjectId','name'],
     * //          'country' => [
     * //              '@' => ['*']
     * //          ]
     * //      ]
     * // ]
     * 
     */
    public function parseSelectStructure()
    {
        $splitArray = preg_split('/(\(|\)|,)/U', $this->select, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $struct = [];
        $refStack[] = &$struct;
        $stackPointer = 0;
        $lastKey = NULL;
        foreach($splitArray as $piece) {
            $piece = trim($piece);
            switch($piece) {
                case ',':
                    break;
                case '(':
                    $refStack[$stackPointer][$lastKey] = [];
                    $refStack[] = &$refStack[$stackPointer][$lastKey];
                    $stackPointer++;
                    break;
                case ')':
                    array_pop($refStack);
                    $stackPointer--;
                    break;
                default:
                    $refStack[$stackPointer][$piece] = TRUE;
                    $lastKey = $piece;
                    break;
            }
        }
        $this->selectStructure = $struct;
        
//        $splitArray = preg_split('/(\(|\)|,)/U', $this->select, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
//        $struct = ['@' => []];
//        $refStack[] = &$struct;
//        $index = 0;
//        foreach($splitArray as $piece) {
//            switch($piece) {
//                case ',':
//                    continue;
//                case '(':
//                    $lastSelect = array_pop($refStack[$index]['@']);
//                    $subStruct = ['@' => []];
//                    $refStack[$index][$lastSelect] = $subStruct;
//                    $refStack[] = &$refStack[$index][$lastSelect];
//                    $index++;
//                    continue;
//                case ')':
//                    array_pop($refStack);
//                    $index--;
//                    continue;
//                default:
//                    $refStack[$index]['@'][] = $piece;
//            }
//        }
//        $this->selectStructure = $struct;
    }

    
    public static function apiConstruct($data)
    {
        return self::create($data);
    }

}