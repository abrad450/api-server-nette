<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Sort Array List
 */
class SortArrayList extends Nette\Utils\ArrayList
{
    
    public function offsetSet($index, $value): void
    {
        if(!($value instanceof Sort)) {
            throw new Nette\InvalidArgumentException('Only instances of '.Sort::class.' can be added');
        }
        parent::offsetSet($index, $value);
    }
    
    
    public function prepend($value): void
    {
        if(!($value instanceof Sort)) {
            throw new Nette\InvalidArgumentException('Only instances of '.Sort::class.' can be added');
        }
        parent::prepend($value);
    }
    
}
