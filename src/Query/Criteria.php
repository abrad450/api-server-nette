<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Criteria
 * 
 * @property string $search Search
 * @property int $page Page no.
 * @property int $pageSize Page size
 * @property SortArrayList $sort Page size
 * @property FilterGroup $filter Page size
 */
class Criteria
{
    use Nette\SmartObject;
    
    /** 
     * Search string
     * 
     * @var string
     */
    protected $search;
    
    /**
     * Page number
     * 
     * @var int
     */
    protected $page;
    
    /**
     * Page size
     * 
     * @var int
     */
    protected $pageSize;
    
    /**
     * Sort array
     * 
     * @var SortArrayList
     */
    protected $sort;
    
    /**
     * Filter group
     * 
     * @var FilterGroup
     */
    protected $filter;
    
    
    public function __construct()
    {
        $this->sort = new SortArrayList();
    }
    
    
    public function getSearch(): ?string
    {
        return $this->search;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function getSort(): ?SortArrayList
    {
        return $this->sort;
    }

    public function getFilter(): ?FilterGroup
    {
        return $this->filter;
    }

    public function setSearch(?string $search)
    {
        $this->search = $search;
        return $this;
    }

    public function setPage(?int $page)
    {
        if($page <= 0) {
            $page = 1;
        }
        $this->page = $page;
        return $this;
    }

    public function setPageSize(?int $pageSize)
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    public function setSort(?SortArrayList $sort)
    {
        $this->sort = $sort;
        return $this;
    }

    public function setFilter(?FilterGroup $filter)
    {
        $this->filter = $filter;
        return $this;
    }
    
    
    /**
     * Create Criteria structure from given data
     * 
     * @param array|object $data
     * @return self
     */
    public static function create($data)
    {
        $criteria = new static();
        foreach($data as $prop => $value) {
            if($value === NULL) {
                continue;
            }
            switch(strtolower($prop)) {
                case 'search':
                    $criteria->setSearch($value);
                    break;
                case 'page':
                    $criteria->setPage($value);
                    break;
                case 'pagesize':
                    $criteria->setPageSize($value);
                    break;
                case 'sort':
                    $sort = is_array($value) && is_int(key($value)) ? $value : [$value];
                    foreach($sort as $sortItem) {
                        $sortObj = Sort::create($sortItem);
                        if($sortObj->field) {
                            $criteria->sort[] = $sortObj;
                        }
                    }
                    break;
                case 'filter':
                    $criteria->setFilter(FilterGroup::create($value));
                    break;
            }
        }
        
        return $criteria;
    }
    
}
