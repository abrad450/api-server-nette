<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Filter
 * 
 * @property string $field Field
 * @property string $operator Operator
 * @property mixed $value value
 */
class Filter
{
    use Nette\SmartObject;
    
	/**
	 * Pole aliasu operatoru
	 * @var array
	 */
	protected static $operators = [

		// Equal To ("eq", "==", "isequalto", "equals", "equalto", "equal")
		'='                         => '=',
		'=='                        => '=',
		'eq'                        => '=',
		'isequalto'                 => '=',
		'equals'                    => '=',
		'equalto'                   => '=',
		'equal'                     => '=',

		// Not Equal to ("neq", "!=", "isnotequalto", "notequals", "notequalto", "notequal", "ne")
		'<>'                        => '<>',
		'!='                        => '<>',
		'neq'                       => '<>',
		'isnotequalto'              => '<>',
		'notequals'                 => '<>',
		'notequalto'                => '<>',
		'notequal'                  => '<>',
		'ne'                        => '<>',

		// Less Than ("lt", "<", "islessthan", "lessthan", "less")
		'<'                         => '<',
		'lt'                        => '<',
		'islessthan'                => '<',
		'lessthan'                  => '<',
		'less'                      => '<',

		// Greater Than ("gt", ">", "isgreaterthan", "greaterthan", "greater")
		'>'                         => '>',
		'gt'                        => '>',
		'isgreaterthan'             => '>',
		'greaterthan'               => '>',
		'greater'                   => '>',

		// Less Than or Equal To ("lte", "<=", "islessthanorequalto", "lessthanequal", "le")
		'<='                        => '<=',
		'lte'                       => '<=',
		'islessthanorequalto'       => '<=',
		'lessthanequal'             => '<=',
		'le'                        => '<=',

		// Greater Then or Equal To ("gte", ">=", "isgreaterthanorequalto", "greaterthanequal", "ge")
		'>='                        => '>=',
		'gte'                       => '>=',
		'isgreaterthanorequalto'    => '>=',
		'greaterthanequal'          => '>=',
		'ge'                        => '>=',

		// String operators
		'starts'                    => 'startswith',
		'startswith'                => 'startswith',
		'ends'                      => 'endswith',
		'endswith'                  => 'endswith',
		'containing'                => 'contains',
		'contains'                  => 'contains',
		'substring'                 => 'contains',
		'substringof'               => 'contains',
		'doesnotcontain'            => 'notcontains',

		// in + not in
		'in'                        => 'in',
		'not in'                    => 'not in',
        
		// cislo tydne + aktualni tyden
		'isweek'                    => 'isweek',
		'thisweek'                  => 'thisweek',
		'today'                     => 'today',
        
		// others
		'is null'                   => 'is null',
		'isnull'                    => 'is null',
		'null'                      => 'is null',
		'isempty'                   => 'is null',
		'is not null'               => 'is not null',
		'isnotnull'                 => 'is not null',
		'notnull'                   => 'is not null',
		'isnotempty'                => 'is not null',
	];    
    
    
    /**
     * Field
     * 
     * @var string
     */
    protected $field;
    
    /**
     * Operator
     * 
     * @var string
     */
    protected $operator;
    
    /**
     * Value
     * 
     * @var mixed
     */
    protected $value;
    
    
    public function getField(): ?string
    {
        return $this->field;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setField(?string $field)
    {
        $this->field = $field;
        return $this;
    }

    public function setOperator(string $operator)
    {
        $op = strtolower($operator);
        if(!isset(self::$operators[$op])) {
            throw new Nette\InvalidArgumentException('Invalid operator: '.$operator);
        }
        $this->operator = self::$operators[$op];
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function __construct()
    {
        $this->operator = '=';
    }
    
    /**
     * Create FilterGroup structure from given data
     * 
     * @param array|object $data
     * @return self
     */
    public static function create($data)
    {
        $filter = new static();
        foreach($data as $prop => $value) {
            switch(strtolower($prop)) {
                case 'field':
                    $filter->setField(trim($value));
                    break;
                case 'operator':
                    $filter->setOperator(trim($value));
                    break;
                case 'value':
                    $filter->setValue($value);
                    break;
            }
        }
        return $filter;
    }
    
}
