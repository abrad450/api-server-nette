<?php

namespace AbraD450\ApiServer\Query;

use Nette;
use Nette\Database\Row;

/**
 * Process Row Event Data
 * 
 * @property bool $shouldSkip
 * @property-read Row $row
 * @property-read array $colPropMap
 */
class ProcessRowEventData
{
    use Nette\SmartObject;
    
    private bool $shouldSkip = FALSE;
    
    private Row $row;
    
    private array $colPropMap;
    
    
    public function __construct(Row $row, array &$colPropMap)
    {
        $this->row = $row;
        $this->colPropMap = &$colPropMap;
    }
    
    
    public function getRow(): Row
    {
        return $this->row;
    }
    
    public function &getColPropMap(): array
    {
        return $this->colPropMap;
    }
        
    
    public function getShouldSkip(): bool
    {
        return $this->shouldSkip;
    }
    
    public function setShouldSkip(bool $shouldSkip)
    {
        $this->shouldSkip = $shouldSkip;
    }
}
