<?php

namespace AbraD450\ApiServer\Query;

use Nette;

use AbraD450\MappedDatabase\Entity\Entity;

/**
 * Processor Event Data
 * 
 * @property bool $shouldSkip Should skip the whole entity
 * @property-read Entity $entity
 * @property-read array $selectStructure
 */
class ProcessEntityEventData extends ProcessorEventData
{
    use Nette\SmartObject;
    
    private bool $shouldSkip = FALSE;
    
    private Entity $entity;
    
    private array $selectStructure;
    
    
    public function __construct(Entity $entity, array &$selectStructure)
    {
        $this->entity = $entity;
        $this->selectStructure = &$selectStructure;
    }
    
    
    public function getEntity(): Entity
    {
        return $this->entity;
    }
    
    public function &getSelectStructure(): array
    {
        return $this->selectStructure;
    }
        
    
    public function getShouldSkip(): bool
    {
        return $this->shouldSkip;
    }
    
    public function setShouldSkip(bool $shouldSkip)
    {
        $this->shouldSkip = $shouldSkip;
    }
}