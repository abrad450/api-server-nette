<?php

namespace AbraD450\ApiServer\Query;

use Nette\Utils\ArrayHash;
use Nette\Utils\ArrayList;

use AbraD450\MappedDatabase\Entity\Entity;

/**
 * Processed Event Data
 * 
 * @property-read ArrayHash $set Set fields to output
 * @property-read ArrayList $unset Unset fields from output
 * @property-read array $output Output
 */
class ProcessedEntityEventData extends ProcessEntityEventData
{
    private ArrayHash $set;
            
    private ArrayList $unset;
        
    private array $output;
    
    public function __construct(Entity $entity, array &$selectStructure, array &$output)
    {
        parent::__construct($entity, $selectStructure);
        $this->set = new ArrayHash();
        $this->unset = new ArrayList();
        $this->output = &$output;
    }
    
    public function getSet(): ArrayHash
    {
        return $this->set;
    }
    
    public function getUnset(): ArrayList
    {
        return $this->unset;
    }
    
    
    public function &getOutput(): array
    {
        return $this->output;
    }
}