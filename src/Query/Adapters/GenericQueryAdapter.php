<?php

namespace AbraD450\ApiServer\Query\Adapters;

use Nette\Database\Table\Selection;
use Nette\Utils\Strings;

class GenericQueryAdapter implements QueryAdapter
{
    /**
     * @inheritDoc
     */
    public function applyWhere(Selection $selection, string $sql, array $params): void
    {
        $selection->where($sql, ...$params);
    }
    
    /**
     * @inheritDoc
     */
    public function applyLimitOffset(Selection $selection, ?int $limit, ?int $offset): void
    {
        $selection->limit($limit, $offset);
    }

    /**
     * @inheritDoc
     */    
    public function applyOrder(Selection $selection, string $column, string $dir): void
    {
        $selection->order($column.' '.($dir === 'DESC' ? 'DESC' : 'ASC'));        
    }
    
    /**
     * @inheritDoc
     */
    public function applySearch(Selection $selection, array $searchables, string $search): void
    {
        $selection->where(
            "LOWER(".implode(') LIKE ? OR LOWER(', $searchables).') LIKE ?',
            ...array_fill(0, count($searchables), '%'. Strings::lower($search).'%')
        );
    }
    
    /**
     * @inheritDoc
     */
    public function valueForFilter(mixed $value, string $type, string $nativeType): string
    {
        if($type === \DateTime::class || $type === "\\".\DateTime::class) {

            $wasDateOnly = false;
            $val = $this->parseDateTimeValue($value, $wasDateOnly);
            
            if($nativeType === 'date') {
                return $val->format('Y-m-d');
            }
            
            if($nativeType === 'time') {
                return $val->format('H:i:s');
            }
            
            if($nativeType === 'datetime' || $nativeType === 'timestamp') {
                return $val->format('Y-m-d H:i:s');
            }
        }
        
        return (string)$value;
    }
    
    /**
     * @inheritDoc
     */
    public function valueForOutput(mixed $value, ?string $type, ?string $nativeType, array $formats): mixed
    {
        if($value === null) {
            return null;
        }
        
        if($type === \DateTime::class || $type === "\\".\DateTime::class && !($value instanceof \DateTimeInterface)) {
            $wasDateOnly = false;
            $value = $this->parseDateTimeValue($value, $wasDateOnly);
        }
        
        // DateTime
        if($value instanceof \DateTimeInterface) {
            return match($nativeType) {
                'date' => $value->format($formats['date']),
                'datetime' => $value->format($formats['datetime']),
                'time' => $value->format($formats['time']),
                default => $value->format(DATE_ATOM)
            };
        }
        
        // DateInterval
        if($value instanceof \DateInterval) {
            return $value->format($formats['timeinterval']);
        }

        return $value;
    }
    
    
    /**
     * @inheritDoc
     */
    public function parseDateTimeValue(string|\DateTimeInterface $val, bool &$dateOnly = false): \DateTimeInterface|bool
    {
        if($val instanceof \DateTime) {
            return $val;
        }
        
        foreach([DATE_ATOM, DATE_COOKIE, DATE_ISO8601, DATE_RFC1036, DATE_RFC1123, DATE_RFC3339, DATE_RFC7231, DATE_RSS, 'Y-m-d H:i:s', 'Y-m-d H:i'] as $format) {
            if(($dateObj = \DateTime::createFromFormat($format, $val)) !== FALSE) {
                $dateObj->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                return $dateObj;
            }
        }
        
        if(($dateObj = \DateTime::createFromFormat('Y-m-d', $val)) !== FALSE) {
            $dateOnly = true;
            return $dateObj;
        }
        
        if(($dateObj = new \DateTime($val)) !== FALSE) {
            $dateObj->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            return $dateObj;
        }
        
        return false;
    }
    
}