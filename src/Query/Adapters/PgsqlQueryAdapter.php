<?php

namespace AbraD450\ApiServer\Query\Adapters;

use Nette\Database\Table\Selection;
use Nette\Utils\Strings;

/**
 * Postgres Query Adapter
 */
class PgsqlQueryAdapter extends GenericQueryAdapter
{
    /**
     * @inheritDoc
     */
    public function applySearch(Selection $selection, array $searchables, string $search): void
    {
        $searchablesCasted = array_map(fn($searchable) => "{$searchable}::varchar", $searchables);
        $selection->where(
            "LOWER(".implode(') LIKE ? OR LOWER(', $searchablesCasted).') LIKE ?',
            ...array_fill(0, count($searchablesCasted), '%'. Strings::lower($search).'%')
        );
    }
}
