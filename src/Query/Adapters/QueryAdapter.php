<?php

namespace AbraD450\ApiServer\Query\Adapters;

use Nette\Database\Table\Selection;

interface QueryAdapter
{

    /**
     * Apply where
     * 
     * @param Selection $selection
     * @param string $sql SQL query
     * @param array $params Params array
     * @return void
     */
    public function applyWhere(Selection $selection, string $sql, array $params): void;
    
    /**
     * Apply limit + offset
     * 
     * @param Selection $selection
     * @param int|null $limit
     * @param int|null $offset
     * @return void
     */
    public function applyLimitOffset(Selection $selection, ?int $limit, ?int $offset): void;
       
    /**
     * Apply order
     * 
     * @param Selection $selection
     * @param string $column Column name (entire PATH)
     * @param string $dir ASC / DESC in uppercase
     * @return void
     */
    public function applyOrder(Selection $selection, string $column, string $dir): void;
    
    /**
     * Apply search on selection 
     * 
     * @param Selection $selection DB Selection
     * @param array $searchables Searchables (columns)
     * @param string $search Search text
     * @return void
     */
    public function applySearch(Selection $selection, array $searchables, string $search): void;
    
    
    /**
     * Prepare $value to be used as a string value in SQL filter
     * 
     * @param mixed $value
     * @param string $type Type (for example \DateTime)
     * @param string $nativeType (for example time)
     * @return string
     */
    public function valueForFilter(mixed $value, string $type, string $nativeType): string;
    
    
    /**
     * Prepare $value to be used as an output value
     * 
     * @param mixed $value
     * @param string $type
     * @param string $nativeType
     * @param array $formats 'date', 'time', 'datetime', 'timeinterval'
     * @return mixed
     */
    public function valueForOutput(mixed $value, ?string $type, ?string $nativeType, array $formats): mixed;

    
    /**
     * Parse DateTime value
     * 
     * @param string|\DateTimeInterface $val
     * @param bool $dateOnly
     * @return \DateTimeInterface|bool
     */
    public function parseDateTimeValue(string|\DateTimeInterface $val, bool &$dateOnly = false): \DateTimeInterface|bool;
}