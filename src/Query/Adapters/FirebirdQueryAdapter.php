<?php

namespace AbraD450\ApiServer\Query\Adapters;

use Nette\Database\Table\Selection;
use Nette\Database\SqlLiteral;
use Nette\Utils\Strings;

/**
 * Firebird Query Adapter
 */
class FirebirdQueryAdapter extends GenericQueryAdapter
{
    /**
     * Firebird needs to cast the search fields to BLOB, it will fail otherwise
     * 
     * @param Selection $selection
     * @param array $searchables
     * @param string $search
     * @return void
     */
    public function applySearch(Selection $selection, array $searchables, string $search): void
    {
        $selection->where(
            'CAST(COALESCE("'.implode('",\'\')||\' \'||COALESCE("', $searchables).'",\'\') AS BLOB SUB_TYPE 1) CONTAINING ?',
            new SqlLiteral('?', [Strings::lower($search)])
//            'CAST("'.implode('" AS BLOB SUB_TYPE 1) CONTAINING ? OR CAST("', $searchables).'" AS BLOB SUB_TYPE 1) CONTAINING ?',
//            ...array_fill(0, count($searchables), new SqlLiteral('?', [Strings::lower($search)]))
        );        
    }
}
