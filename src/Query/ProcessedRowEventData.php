<?php

namespace AbraD450\ApiServer\Query;

use Nette\Utils\ArrayHash;
use Nette\Utils\ArrayList;

use Nette\Database\Row;

/**
 * Processed Row Event Data
 * 
 * @property-read ArrayHash $set Set fields to output
 * @property-read ArrayHash $unset Unset fields from output
 * @property-read array $output Output
 */
class ProcessedRowEventData extends ProcessRowEventData
{
    private ArrayHash $set;
            
    private ArrayList $unset;
        
    private array $output;
    
    public function __construct(Row $row, array &$colPropMap, array &$output)
    {
        parent::__construct($row, $colPropMap);
        $this->set = new ArrayHash();
        $this->unset = new ArrayList();
        $this->output = &$output;
    }
    
    public function getSet(): ArrayHash
    {
        return $this->set;
    }
    
    public function getUnset(): ArrayList
    {
        return $this->unset;
    }
    
    
    public function &getOutput(): array
    {
        return $this->output;
    }

}