<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Filter/FilterGroup Array List
 */
class FilterArrayList extends Nette\Utils\ArrayList
{
    
    public function offsetSet($index, $value): void
    {
        if(!($value instanceof Filter || $value instanceof FilterGroup)) {
            throw new Nette\InvalidArgumentException('Only instances of '.Filter::class.' or '.FilterGroup::class.' can be added');
        }
        parent::offsetSet($index, $value);
    }
    
    
    public function prepend($value): void
    {
        if(!($value instanceof Filter || $value instanceof FilterGroup)) {
            throw new Nette\InvalidArgumentException('Only instances of '.Filter::class.' or '.FilterGroup::class.' can be added');
        }
        parent::prepend($value);
    }
    
}
