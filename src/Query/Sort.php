<?php

namespace AbraD450\ApiServer\Query;

use Nette;

/**
 * Sort
 * 
 * @property string $field Field
 * @property string $dir Dir
 */
class Sort
{
    use Nette\SmartObject;

    /**
     * Sort field
     * 
     * @var string
     */
    protected $field;
    
    /**
     * Sort direction
     * 
     * @var string
     */
    protected $dir;
    
    
    public function getField(): ?string
    {
        return $this->field;
    }

    public function getDir(): ?string
    {
        return $this->dir;
    }

    public function setField(?string $field)
    {
        $this->field = $field;
        return $this;
    }

    public function setDir(?string $dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * Create Sort structure from given data
     * 
     * @param array|object $data
     * @return self
     */
    public static function create($data)
    {
        $sort = new static();
        foreach($data as $prop => $value) {
            switch(strtolower($prop)) {
                case 'field':
                    $sort->setField(trim($value));
                    break;
                case 'dir':
                    $sort->setDir(trim($value));
                    break;
            }
        }
        return $sort;
    }
    
}
