<?php

namespace AbraD450\ApiServer;

use Nette;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Caching\Storage;
use Nette\Caching\Cache;
use Nette\Security\User as NSUser;

use AbraD450\ApiServer;

/**
 * Base Server
 */
abstract class BaseServer
{
    use Nette\SmartObject;
    
    /**
     * Default values
     */
    static protected array $defaults = [
        'dateTimeFormats' => [
            \DateTimeInterface::ATOM,
            \DateTimeInterface::COOKIE,
            \DateTimeInterface::ISO8601,
            // \DateTimeInterface::ISO8601_EXPANDED,
            \DateTimeInterface::RFC1036,
            \DateTimeInterface::RFC1123,
            \DateTimeInterface::RFC3339,
            \DateTimeInterface::RFC3339_EXTENDED,
            \DateTimeInterface::RFC7231,
            \DateTimeInterface::RFC850,
            \DateTimeInterface::RSS,
            \DateTimeInterface::W3C,
            'Y-m-d',
            'Y-m-d H:i:s',
            'Y-m-d H:i',
            "Y-m-d\\TH:i:s"
        ]
    ];
    
    /**
     * Configuration array
     */
    protected array $config;
    
    /**
     * Cache
     */
    protected Cache $cache;
    
    /**
     * Configuration array
     * 
     * @return array
     */
    public function &getConfig()
    {
        return $this->config;
    }

    /**
     * Construct
     *
     * @param array $config
     * @param Container $dic
     * @param IRequest $request
     * @param IResponse $response
     * @param Storage $cacheStorage
     * @param NSUser $user
     */
    public function __construct(
            array $config, 
            protected Container $dic, 
            protected IRequest $request, 
            protected IResponse $response,
            protected Storage $cacheStorage,
            protected NSUser $user
        )
    {
        $this->config = Nette\DI\Config\Helpers::merge($config, self::$defaults);
        $this->cache = new Nette\Caching\Cache($this->cacheStorage, str_replace('\\', '.', __NAMESPACE__));
    }
    
    
    
    // -------------------------------------------------------------------------
    
    
    /**
     * Build authorize handler value
     * 
     * @param array $rule
     * @param array $mRule
     * @return bool|array
     */
    protected function buildHandlerAuthorize(array $rule, array $mRule): bool|array
    {
        $cAuthorize = $rule['authorize'] ?? null;
        $mAuthorize = $mRule['authorize'] ?? null;
        if(!empty($mAuthorize['allowedAnonymous'])) {
            return false; // no authorization required
        }
        
        // method has NO Authorize defined
        if(empty($mAuthorize) || $mAuthorize['on'] === false) {
            if(empty($cAuthorize) || $cAuthorize['on'] === false) {
                return false;
            }
            
            if(empty($cAuthorize['roles'])) {
                return true;
            }
            
            return ['roles' => $cAuthorize['roles']];
        }
        
        // method has Authorize defined        
        $mRoles = $mAuthorize['roles'] ?? [];
        
        if(empty($cAuthorize) || $cAuthorize['on'] === false || empty($cAuthorize['roles'])) {
            return empty($mRoles) ? true : ['roles' => $mRoles];
        }
                
        $cRoles = $cAuthorize['roles'] ?? [];
        
        return ['roles' => array_unique(array_merge($mRoles, $cRoles))];
    }

    
    
    // -------------------------------------------------------------------------
    
    
    
    /**
     * Send Access-Control-* headers based on endpoint configuration
     * 
     * @param string $endPoint
     * @return void
     */
    protected function sendAccessControlHeaders(string $endPoint): void
    {
        $config = &$this->config[$endPoint];
        if(empty($config['accessControl'])) {
            return;
        }
        
        // Get ORIGIN
        $origin = $this->request->getHeader('origin');
        if(empty($origin)) {
            return;
        }

        $ac = &$config['accessControl'];

        if(!empty($ac['allowedOrigins']) && is_array($ac['allowedOrigins'])) {
            $origin = in_array($origin, $ac['allowedOrigins']) || in_array('*', $ac['allowedOrigins']) ? $origin : '?';
        }
        
        $this->response->addHeader('Access-Control-Allow-Origin', $origin);
        $this->response->addHeader('Access-Control-Allow-Credentials',
                (empty($ac['allowCredentials']) ? 'false' : 'true'));
        $this->response->addHeader('Access-Control-Allow-Methods', 
                (empty($ac['allowMethods']) ? 'POST, OPTIONS' : implode(', ', $ac['allowMethods'])));
        $this->response->addHeader('Access-Control-Allow-Headers',
                (empty($ac['allowHeaders']) ? 'origin, content-type, accept, x-tracy-ajax' : implode(', ', $ac['allowHeaders'])));
        $this->response->addHeader('Access-Control-Expose-Headers',
                (empty($ac['exposeHeaders']) ? 'origin, location, content-type, accept, x-tracy-ajax' : implode(', ', $ac['exposeHeaders'])));
        $this->response->addHeader('Access-Control-Max-Age',
                (empty($ac['maxAge']) ? "1728000" : (string)$ac['maxAge']));
    }
    
    /**
     * Process OPTIONS request
     * 
     * @param string $endPoint
     */
    protected function processOptions(string $endPoint): void
    {
        $this->sendAccessControlHeaders($endPoint);
        $this->sendResponse(null);
    }


    /**
     * Build parameters
     * 
     * @param ApiController $service
     * @param string $method
     * @param array $parameters
     * @return type
     * @throws InvalidParamsException
     */
    protected function buildParameters(ApiController $service, string $method, array $parameters = [])
    {
        // Prepare parameters
        $refMethod = new \ReflectionMethod($service, $method);
        $refParams = $refMethod->getParameters();
        $paramArr = [];
        foreach($refParams as $refParam) {
            /* @var $refParam \ReflectionParameter */
            $refType = $refParam->getType();
            $paramName = $refParam->getName();
            $paramPosition = $refParam->getPosition();
            $paramIdent = $paramName ?? '#'.$paramPosition;
                        
            // Mix values by index/names with defaults
            $value = $refParam->isDefaultValueAvailable() ? $refParam->getDefaultValue() : NULL;
            if(array_key_exists($paramName, $parameters)) {
                $paramIdent = $paramName;
                $value = $parameters[$paramName];
            }
            elseif(array_key_exists($paramPosition, $parameters)) {
                $value = $parameters[$paramPosition];
            }
            
            // No parameter type? Set value and continue to next param
            if($refType === NULL) {
                $paramArr[] = $value;
                continue;
            }
            
            // nulls allowed + value is null
            if($refType->allowsNull() && $value === null) {
                $paramArr[] = $value;
                continue;
            }            
            
            // check types and construct objects
            if($refType instanceof \ReflectionUnionType) {
                $unionTypes = $refType->getTypes();
                foreach($unionTypes as $uType) {
                    try {
                        if($uType->isBuiltin()) {
                            $value = $this->buildBuiltinValue($uType, $paramIdent, $value);
                            break;
                        }
                        else {
                            $value = $this->buildNotBuiltinValue($uType, $paramIdent, $value);
                            break;
                        }
                    }
                    catch(InvalidParamsException $ipe) {
                        continue;
                    }
                }
            }
            else {
                /* @var $refType \ReflectionNamedType */
                if($refType->isBuiltin()) {
                    $value = $this->buildBuiltinValue($refType, $paramIdent, $value);
                }
                else {
                    $value = $this->buildNotBuiltinValue($refType, $paramIdent, $value);
                }
            }
            $paramArr[] = $value;
        }
        return $paramArr;
    }
    
    protected function buildBuiltinValue(\ReflectionNamedType $refType, string $paramIdent, mixed $value)
    {
        $refTypeName = $refType->getName();
        switch($refTypeName) {
            
            // try parse JSON object if value is a string
            case 'object':
                if(is_string($value)) {
                    try {
                        $value = Nette\Utils\Json::decode($value, false);
                    }
                    catch(Nette\Utils\JsonException $e) {
                        throw new InvalidParamsException("Parameter '{$paramIdent}' must be '{$refTypeName}' but ".self::getTypeOfValue($value)." given.", previous: $e);
                    }
                }
                break;
            
            // callable cannot be used
            case 'callable':
                throw new InvalidParamsException("Type '{$refTypeName}' is not supported for API method parameters.");
            
            case 'int':
                if(!is_scalar($value) || !is_numeric($value)) {
                    throw new InvalidParamsException("Parameter '{$paramIdent}' must be '{$refTypeName}' but ".self::getTypeOfValue($value)." given.");
                }
                $value = intval($value);
                break;
                
            case 'float':
            case 'double':
                if(!is_scalar($value) || !is_numeric($value)) {
                    throw new InvalidParamsException("Parameter '{$paramIdent}' must be '{$refTypeName}' but ".self::getTypeOfValue($value)." given.");
                }
                $value = floatval($value);
                break;
                
            case 'string':
                if(!is_scalar($value)) {
                    throw new InvalidParamsException("Parameter '{$paramIdent}' must be '{$refTypeName}' but ".self::getTypeOfValue($value)." given.");
                }
                break;
                
            case 'bool':
                if(!is_scalar($value) || !in_array($value, [1, 0, '1', '0', 'true', 'false', true, false], true)) {
                    throw new InvalidParamsException("Parameter '{$paramIdent}' must be '{$refTypeName}' but ".self::getTypeOfValue($value)." given.");
                }
                $value = in_array($value, [1, '1', 'true', true], true);
                break;
        }
        
        return $value;
    }
    
    protected function buildNotBuiltinValue(\ReflectionNamedType $refType, string $paramIdent, mixed $value): mixed
    {
        $refTypeName = $refType->getName();
        
        // DateTime
        $dateTime = [
            \DateTime::class => \DateTime::class,
            \DateTimeImmutable::class => \DateTimeImmutable::class,
            \DateTimeInterface::class => \DateTime::class
        ];
        if(isset($dateTime[$refTypeName])) {
            $cls = $dateTime[$refTypeName];
            $result = $this->buildDateTime($cls, $value);
            if($result === false) {
                throw new InvalidParamsException("Parameter '{$paramIdent}' must be valid DateTime string. Value '{$value}' is not valid.");
            }
            return $result;
        }

        // ApiConstructible
        if(!is_subclass_of($refTypeName, ApiServer\ApiConstructible::class)) {
            throw new InvalidParamsException("Parameter '{$paramIdent}' must be instance of '{$refTypeName}'. Check that '{$refTypeName}' implements the ".ApiConstructible::class." interface.");
        }

        return $refTypeName::apiConstruct($value);
    }
    
    
    /**
     * Try to build DateTime(Immutable) object from $value
     * 
     * @param string $class
     * @param string $value
     * @return \DateTimeInterface|false
     */
    protected function buildDateTime($class, $value): \DateTimeInterface|false
    {
        foreach($this->config['dateTimeFormats'] as $dtf) {
            $result = $class::createFromFormat($dtf, $value);
            if($result !== false) {
                return $result;
            }
        }
        $result = new $class($value);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    // -------------------------------------------------------------------------
    
    
    /**
     * Helper method for obtaining $value's type
     * 
     * @param mixed $value
     * @return string
     */
    static protected function getTypeOfValue($value)
    {
        $type = gettype($value);
        if($type === 'array') {
            return 'array/object';
        }
        return $type;
    }    
    
}
