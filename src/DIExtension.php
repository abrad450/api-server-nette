<?php

namespace AbraD450\ApiServer;

use Nette;
use Nette\Schema\Expect;

/**
 * DI Extension
 */
class DIExtension extends Nette\DI\CompilerExtension
{
    
	public function getConfigSchema(): Nette\Schema\Schema
	{
		return Expect::structure([
            'query' => Expect::structure([
                'dateTimeFormat' => Expect::string(),
                'dateFormat' => Expect::string(),
                'timeFormat' => Expect::string(),
                'timeIntervalFormat' => Expect::string(),
                'maxPageSize' => Expect::int(),
                'pagination' => Expect::anyOf(Expect::string(), Expect::bool())
            ])->skipDefaults()->castTo('array'),
            'rpc' => Expect::arrayOf('mixed'),
            'rest' => Expect::arrayOf('mixed'),
		]);
	}     

    
    public function loadConfiguration()
    {
        $config = $this->getConfig();        
        $builder = $this->getContainerBuilder();
        /* @var $builder \Nette\DI\ContainerBuilder */
        
        // RPC server
        if(!empty($config->rpc)) {
            $builder->addDefinition($this->prefix("rpcServer"))
                ->setFactory(RPC\Server::class, [$config->rpc])
                ->setAutowired();
        }
        // REST server
        if(!empty($config->rest)) {
            $builder->addDefinition($this->prefix("restServer"))
                ->setFactory(REST\Server::class, [$config->rest])
                ->setAutowired();
            // Open API generator for REST
            $builder->addDefinition($this->prefix("openApiGenerator"))
                ->setFactory(REST\Documentation\OpenAPIGenerator::class)
                ->setAutowired(TRUE);        
        }
        
        // Query processor
        $builder->addDefinition($this->prefix("processor"))
            ->setFactory(Query\Processor::class, [$config->query])
            ->setAutowired(TRUE);
    }
    
    public function afterCompile(Nette\PhpGenerator\ClassType $class)
    {
        $config = $this->getConfig();
        $initialize = $class->getMethod('initialize');
        if(!empty($config->rpc)) {
            $initialize->addBody('$this->getService(\''.$this->prefix("rpcServer").'\')->process();');
        }
        if(!empty($config->rest)) {
            $initialize->addBody('$this->getService(\''.$this->prefix("restServer").'\')->process();');
        }
    }
    
}