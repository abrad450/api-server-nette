<?php

namespace AbraD450\ApiServer\REST\Documentation;

use Nette;
use Nette\Utils\Strings;

use AbraD450\ApiServer\ApiController;

use AbraD450\ApiServer\REST;
use AbraD450\ApiServer\REST\Attributes\Response;

use AbraD450\ApiServer\Common\Attributes\NonApi;
use AbraD450\ApiServer\Common\Attributes\Parameter;

/**
 * API Metadata Parser
 */
class ApiMetaParser
{

    public static function parse(Nette\DI\Container $dic, string $endpoint): array
    {        
        $server = $dic->getByType(REST\Server::class);
        /* @var $server REST\Server */
        
        $epConfig = $server->config[$endpoint];
        $ns = ltrim($epConfig['namespace'], '\\');
        
        $cHandlers = $server->getHandlersCache([$ns]);
        $controllers = [];
        
        foreach($cHandlers as $cController) {
            $service = $dic->getByName($cController['service']);
            $controller = self::processController($service, $cController);
            $controllers[] = $controller;
        }
        
        return $controllers;
    }
    
    protected static function processController(ApiController $controller, array $cController)
    {
        $controllerRef = new \ReflectionClass($controller);
        $controllerComment = self::unwrapDocComment($controllerRef->getDocComment());

        $reflectionMethods = [];
        foreach($controllerRef->getMethods(\ReflectionMethod::IS_PUBLIC) as $methodRef) {
            /* @var $methodRef \ReflectionMethod */
            
            // skip startup method
            if($methodRef->name === 'startup') {
                continue;
            }
            
            // Method from ApiController? skip
            if($methodRef->class === ApiController::class) {
                continue;
            }
            
            // Method is NonApi? skip
            $nonApiAttrs = $methodRef->getAttributes(NonApi::class);
            if(count($nonApiAttrs) > 0) {
                continue;
            }
            
            $reflectionMethods[$methodRef->name] = $methodRef;
        }
        $methods = [];
        foreach($cController['methods'] ?? [] as $cMethod) {
            $methods[] = self::processMethod($reflectionMethods[$cMethod['method']], $cMethod, $cController);
        }
        return [
            'controllerName' => $cController['serviceType'],
            'controllerComment' => $controllerComment['summary'] ?? $controllerComment['comment'],
            'methods' => $methods,
        ];
    }
    
    protected static function processMethod(\ReflectionMethod $methodRef, array $cMethod, array $cController)
    {
        $path = rtrim('/'.trim($cController['simplePath'],'/').'/'.trim($cMethod['simplePath'], '/'), '/');
        
        $docComment = self::unwrapDocComment($methodRef->getDocComment());
        
        // Return from Method Reflection - just defaults
        $returnType = $methodRef->getReturnType();
        if($returnType) {
            $methodReturn = [['code' => 200, 'description' => '200 OK']];
        }
        else {
            $methodReturn = [['code' => 200, 'description' => '200 OK']];
        }
        
        $method = [
            'path' => $path,
            'summary' => $docComment['summary'],
            'comment' => $docComment['comment'],
            'parameters' => self::processMethodParameters($docComment, $methodRef, $cMethod, $cController),
            'return' => $docComment['at']['return'] ?? $methodReturn,
            'httpMethods' => $cMethod['verbs'],
            'security' => self::buildSecurityAttribute($cMethod['authorize'], $cController['authorize'])
        ];

        // Returns attribute
        $responseAttrs = $methodRef->getAttributes(Response::class, \ReflectionAttribute::IS_INSTANCEOF);
        if(!empty($responseAttrs)) {
            $responses = [];
            foreach($responseAttrs as $responseAttr) {
                $resp = $responseAttr->newInstance();
                $responses[] = $resp->getDefinition();
            }
            $method['return'] = $responses;
        }
        
        return $method;
    }
    
    protected static function buildSecurityAttribute(array|null $mAuth, array|null $cAuth): bool
    {
        if(!empty($mAuth['allowedAnonymous'])) {
            return false; // no authorization required
        }
        
        // method has NO Authorize defined
        if(empty($mAuth) || $mAuth['on'] === false) {
            if(empty($cAuth) || $cAuth['on'] === false) {
                return false;
            }
        }
        
        // Method has Auth defined - always true
        return true;
    }
    
    
    protected static function processMethodParameters(array $docComment, \ReflectionMethod $methodRef, array $cMethod, array $cController)
    {
        $params = [
            'route' => [],
            'query' => []
        ];
        
        // Controller ROUTE params
        foreach($cController['params'] as $paramName => $paramDef) {
            $params['route'][$paramName] = [
                'name' => $paramName,
                'type' => $paramDef['dataType'],
                'required' => !$paramDef['optional'],
                'description' => $docComment['at']['param'][$paramName]['description'] ?? ''
            ]; 
        }
        
        // Method ROUTE params
        foreach($cMethod['params'] as $paramName => $paramDef) {
            $params['route'][$paramName] = [
                'name' => $paramName,
                'type' => $paramDef['dataType'],
                'required' => !$paramDef['optional'],
                'description' => $docComment['at']['param'][$paramName]['description'] ?? ''
            ];
        }
        
        // Additional QUERY params from Method Reflection
        $paramsRef = $methodRef->getParameters();
        foreach($paramsRef as $paramRef) {
            /* @var $paramRef \ReflectionParameter */
            $paramName = $paramRef->name;
            
            if(isset($params['route'][$paramName])) {
                continue;
            }
            
            $params['query'][$paramName] = [
                'name' => $paramName,
                'type' => $paramRef->getType(),
                'required' => !$paramRef->isOptional(),
                'description' => $docComment['at']['param'][$paramName]['description'] ?? ''
            ];
        }
        
        // Parameter Attribute
        $paramAttrs = $methodRef->getAttributes(Parameter::class, \ReflectionAttribute::IS_INSTANCEOF);
        if(!empty($paramAttrs)) {
            foreach($paramAttrs as $paramAttr) {
                $pa = $paramAttr->newInstance();
                $paName = $pa->getName();
                // replace to params array
                if(isset($params['route'][$paName])) {
                    $params['route'][$paName]['type'] = $pa->getDefinition();
                }
                elseif(isset($params['query'][$paName])) {
                    $params['query'][$paName]['type'] = $pa->getDefinition();
                }
            }
        }
        
        return $params;
    }
    
    
    protected static function unwrapDocComment(string $docComment): array
    {
        $docCommentLines = explode("\n", $docComment);
        $comment = [];
        $at = [];
        foreach($docCommentLines as $dcLine) {
            $dcLineTrim = trim($dcLine);
            
            if($dcLineTrim === '/**' || $dcLineTrim === '*/') {
                continue;
            }
            if(str_starts_with($dcLineTrim, '/**')) {
                $dcLineTrim = trim(Strings::substring($dcLineTrim, 3));
            }
            if(str_starts_with($dcLineTrim, '*')) {
                $dcLine = Strings::substring($dcLineTrim, 1);
            }
            if(str_ends_with($dcLineTrim, '*/')) {
                $dcLine = trim(Strings::substring($dcLineTrim, 0, -2));
            }
            
            $matches = null;
//            dump($dcLineTrim);
            if(!preg_match('/^\*\s*@([a-z0-9]+)(\s+|$)(.*)$/i', $dcLineTrim, $matches)) {
                $comment[] = $dcLine;
                continue;
            }

            // Parse @param and other @... parts of the doc comment
            if(!isset($at[$matches[1]])) {
                $at[$matches[1]] = [];
            }

            // @param
            if($matches[1] === 'param') {
                $paramMatches = null;
                if(preg_match('/^\*\s*@param\s+([^\s]+)\s+\$([^\s]+)(\s+(.*))?$/is', $dcLineTrim, $paramMatches)) {
                    $at['param'][$paramMatches[2]] = [
                        'name' => $paramMatches[2],
                        'phpName' => '$'.$paramMatches[2],
                        'type' => $paramMatches[1],
                        'description' => $paramMatches[4] ?? ''
                    ];
                }
                continue;
            }
            
            // @return
            if($matches[1] === 'return') {
                $returnMatches = null;
                if(preg_match('/^\*\s*@return\s+([^\s]+)(\s+(.*))?$/is', $dcLineTrim, $returnMatches)) {
                    $at['return'] = [[
                            'code' => 200,
                            'description' => $returnMatches[3] ?? '',
                            'content' => $returnMatches[1]
                        ]];
                }
                continue;
            }

            // currently unprocessed
            $at[$matches[1]][] = $matches[3];
        }
        
        $result = [
            'summary' => empty($comment) ? null : trim(array_shift($comment)),
            'comment' => empty($comment) ? null : trim(implode("\n", $comment)),
            'at' => $at
        ];
                
        return $result;
    }
    
    
}
