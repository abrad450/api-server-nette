<?php

namespace AbraD450\ApiServer\REST\Documentation;

use Nette;
use Nette\Utils\Strings;
use Nette\Utils\FileSystem;

use Symfony\Component\Yaml\Yaml;

use AbraD450\ApiServer\REST;
use AbraD450\ApiServer\Query\Query;

use AbraD450\MappedDatabase\Entity\Entity;
use AbraD450\MappedDatabase\Entity\EntityMetaStorage;

/**
 * OpenAPI Generator
 * 
 * @fixme If method parameter is "array", there is no way how to specify array items type :( currenty 'string' is for the items type
 */
class OpenAPIGenerator
{
    private \Nette\DI\Container $dic;
    
    private array $schemas;
    
    private array $paths;
    
    private string $methodPrefix;
    
    public function __construct(\Nette\DI\Container $dic)
    {
        $this->dic = $dic;
        // to initialize Entity Metadata
        $this->dic->getByType(Nette\Database\Connection::class);
    }

    /**
     * Generate OpenAPI docs and save it to defined location
     * 
     * @param string|null $endpoint If not specified, generates for all REST endpoints
     * @param bool $saveYamlFile
     * @param bool $returnContent
     * @return array
     * @throws Nette\InvalidArgumentException
     * @throws Nette\InvalidStateException
     */
    public function generate(?string $endpoint = null, bool $saveYamlFile = true, bool $returnContent = false): array
    {
        $server = $this->dic->getByType(REST\Server::class);
        /* @var $server REST\Server */
        
        if($endpoint !== null && !isset($server->config[$endpoint])) {
            throw new Nette\InvalidArgumentException("Invalid endpoint '{$endpoint}' given!");
        }
        
        $docs = [];
        foreach($server->config as $endp => $epConfig) {

            if($endp === 'dateTimeFormats' || ($endpoint !== null && $endp !== $endpoint)) {
                continue;
            }
            
            if(!isset($epConfig['openapi']['content'])) {
                throw new Nette\InvalidStateException("Missing 'openapi'->'content' key for endpoint '{$endp}'!");
            }

            $data = ApiMetaParser::parse($this->dic, $endp);

            $doc = $epConfig['openapi']['content'];
            
            // fill paths
            $this->paths = [];
            $this->schemas = [];
            $this->methodPrefix = $epConfig['openapi']['pathsPrefix'] ?? '/'.trim($endp, '/');

            foreach($data as $ctl) {
                $this->generateController($ctl);
            }

            $doc['components'] = $doc['components'] ?? [];
            $doc['components']['schemas'] = $this->schemas;

            $doc['paths'] = $doc['paths'] ?? [];
            $doc['paths'] = array_merge($doc['paths'], $this->paths);
            
            $epResult = [];
            if($returnContent) {
                $epResult['content'] = $doc;
            }
            
            // Save to output directory if 'outputPath' is defined 
            if($saveYamlFile && isset($epConfig['openapi']['outputPath'])) {
                $outputPath = rtrim($epConfig['openapi']['outputPath'], '\\/');
                $yamlPath = $outputPath.'/'.Strings::webalize($endp).'.yaml';
                $yamlString = Yaml::dump($doc, 20, 2, Yaml::DUMP_EMPTY_ARRAY_AS_SEQUENCE);
                FileSystem::write($yamlPath, $yamlString);
                $epResult['file'] = $yamlPath;
            }
            
            $docs[$endp] = $epResult;
        }
        
        return $docs;
    }
    
    
    private function generateController(array $ctl): void
    {
        foreach($ctl['methods'] as $method) {
            $this->generateMethod($method,$ctl);
        }
    }
    
    
    private function generateMethod(array $method, array $ctl): void
    {
        $methodVerbs = [];
        foreach(array_keys($method['httpMethods']) as $httpMethod) {
            switch(strtoupper($httpMethod)) {
                case 'GET':
                case 'DELETE':
                case 'HEAD':
                case 'OPTIONS':
                    $methodDefinition = $this->generateGetLikeMethod($method);
                    break;
                    
                case 'POST':
                case 'PUT':
                case 'PATCH':
                default:
                    $methodDefinition = $this->generatePostLikeMethod($method);
                    break;
            }
            
            // add TAG
            $tag = ($ctl['controllerComment'] ?? array_reverse(explode('\\', trim($ctl['controllerName'], '\\')))[0]);
            $methodDefinition['tags'] = [$tag];

            $methodVerbs[strtolower($httpMethod)] = $methodDefinition;
        }
        
        $this->paths[$this->methodPrefix.$method['path']] = $methodVerbs;
    }
    
    
    private function generateGetLikeMethod(array $method): array
    {
        $m = [];
        if(isset($method['summary'])) {
            $m['summary'] = $method['summary'];
        }
        if(isset($method['comment'])) {
            $m['description'] = $method['comment'];
        }
        if(empty($method['security'])) {
            $m['security'] = [];
        }
        
        
        $params = $this->getPathParameters($method);
        
//          schema:
//            type: 'object'
//            properties:
//                query:
//                    $ref: '#/components/schemas/API_Query'        
                
        // Add Query parameters
        foreach($method['parameters']['query'] ?? [] as $param) {
            
            $paramDef = $this->getSchemaDefinition($param['type']);
            
            // Query parameters that are objects, must be defined this way:
            // query[page]=1 ...

            // FIXME: filters array with numeric indexes are not possible @see https://github.com/OAI/OpenAPI-Specification/issues/1501
//            if(isset($paramDef['$ref'])) {
//                $paramDef = [
//                    'type' => 'object',
//                    'properties' => [
//                        $param['name'] => $paramDef
//                    ]
//                ];
//            }
            
            $p = [
                'name' => $param['name'],
                'in' => 'query',
                'description' => $param['description'],
                'required' => $param['required'],
                'schema' => $paramDef
            ];
            // for objects
            if(isset($paramDef['$ref'])) {
                $p['style'] = 'deepObject';
            }
            
            if(!empty($param['required'])) {
                $p['required'] = true;
            }
            
            $params[] = $p;
        }
        
        if(!empty($params)) {
            $m['parameters'] = $params;
        }
        
        if(!empty($method['return'])) {
            $m['responses'] = $this->getResponsesDefinition($method['return']);
        }
        
        return $m;
    }
    
    private function generatePostLikeMethod(array $method): array
    {
        $m = [];
        if(isset($method['summary'])) {
            $m['summary'] = $method['summary'];
        }
        if(isset($method['comment'])) {
            $m['description'] = $method['comment'];
        }
        if(empty($method['security'])) {
            $m['security'] = [];
        }
        
        $params = $this->getPathParameters($method);
        
        // Add Query parameters
        $bodyParams = [];
        $requiredBodyParams = [];
        foreach($method['parameters']['query'] ?? [] as $param) {
            $bodyParams[$param['name']] = $this->getSchemaDefinition($param['type']);
            if($param['required']) {
                $requiredBodyParams[] = $param['name'];
            }
        }
        
        if(!empty($bodyParams)) {
            $m['requestBody'] = [
                'content' => [
                    'application/json' => [
                        'schema' => [
                            'type' => 'object',
                            'properties' => $bodyParams
                        ]
                    ]
                ]
            ];
            if(!empty($requiredBodyParams)) {
               $m['requestBody']['content']['application/json']['schema']['required'] = $requiredBodyParams;
            }
        }
        
        if(!empty($params)) {
            $m['parameters'] = $params;
        }
        
        if(!empty($method['return'])) {
            $m['responses'] = $this->getResponsesDefinition($method['return']);
        }
        
        return $m;
    }
    
    
    private function getPathParameters(array $method): array
    {
        $params = [];
        foreach($method['parameters']['route'] ?? [] as $param) {
            $p = [
                'name' => $param['name'],
                'in' => 'path',
                'description' => $param['description'],
                'required' => true, //$param['required'],
                'schema' => $this->getSchemaDefinition($param['type'])
            ];
            if(empty($param['required'])) {
                throw new Nette\InvalidStateException(
                    "Path parameters must not be optional due to the OpenAPI specification! ".
                    "Split the method '{$method['path']}' into two separate methods. ".
                    "One with parameter '{$param['name']}' and one without it. ".
                    "Another option is to move the parameter '{$param['name']}' from 'path' to 'query'"
                );
            }

            $params[] = $p;
        }
        return $params;
    }
 
    private function getResponsesDefinition(array $return)
    {
        $responses = [];
        
        foreach($return as $definition) {

            $code = $definition['code'];
            
            $resp = [];
            
            // Description
            if(isset($definition['description'])) {
                $resp['description'] = $definition['description'];
            }
            
            if(!empty($definition['content'])) {
                
                
                // Binary data
                if($definition['content'] === '@binary') {
                    $contentType = $definition['contentType'] ?? 'application/octet-stream';
                    $resp['content'] = [
                        $contentType => [
                            'schema' => [
                                'type' => 'string',
                                'format' => 'binary'
                            ]
                        ]
                    ];
                }
                else {
                    // JSON content structure
                    $contentType = $definition['contentType'] ?? 'application/json';
                    $resp['content'] = [
                        $contentType => [
                            'schema' => $this->getResponseContentDefinition($definition['content'])
                        ]
                    ];
                }
            }
             
            $responses[$code] = $resp;
        }
        
        return $responses;
    }
       
    
    private function getSchemaDefinition(mixed $type): array
    {
        if(is_scalar($type)) {
            return $this->getScalarSchemaDefinition($type);
        }
        
        if($type instanceof \ReflectionType) {
            return $this->getReflectionTypeDefinition($type);
        }
        

        $content = $type;

        if(!is_array($content) || count($content) != 2) {
            throw new \Nette\InvalidStateException("Invalid definition");
        }
        
        // Structure
        if($content[0] === 'object') {
            $structProps = [];
            foreach($content[1] as $prop => $propDef) {
                $structProps[$prop] = $this->getResponseContentDefinition($propDef);
            }
            return [
                'type' => 'object',
                'properties' => $structProps
            ];
        }
        
        if($content[0] === 'array') {
            if(is_scalar($content[1])) {
                return [
                    'type' => 'array',
                    'items' => $this->getScalarSchemaDefinition($content[1])
                ];
            }
            return [
                'type' => 'array',
                'items' => $this->getResponseContentDefinition($content[1])
            ];
        }

        // FIXME: object
        return [
            'type' => 'object'
        ];
        
        
    }
    
    
    private function getScalarSchemaDefinition(string $type, array $arrayType = ['type' => 'string']): array
    {
        if(class_exists($type)) {
            
            if($type === Query::class) {
                $refName = $this->getQueryObjectDefinition();
                return ['$ref' => '#/components/schemas/'.$refName];
            }

            if(is_subclass_of(ltrim($type, '\\'), Entity::class)) {
                $refName = $this->getEntityObjectDefinition($type);
                return ['$ref' => '#/components/schemas/'.$refName];
            }
            
            // FIXME: Generic object ?
            return [
                'type' => 'object'
            ];
        }
        
        $t = [
            'type' => match($type) {
                'int' => 'integer',
                'integer' => 'integer',
                'string' => 'string',
                'double' => 'number',
                'float' => 'number',
                'bool' => 'boolean',
                'boolean' => 'boolean',
                'array' => 'array',
                'object' => 'object',
                default => 'string'
            }
        ];
        
        if($t['type'] === 'array') {
            $t['items'] = $arrayType;
        }
        
        return $t;
    }
    
    private function getReflectionTypeDefinition(\ReflectionType $type): array
    {
        if($type instanceof \ReflectionNamedType) {
            return $this->getReflectionNamedTypeDefinition($type);
        }
        
        if($type instanceof \ReflectionUnionType) {
            return $this->getReflectionUnionTypeDefinition($type);
        }
        
        if($type instanceof \ReflectionIntersectionType) {
            return $this->getReflectionIntersectionTypeDefinition($type);
        }
        
        throw new \Nette\InvalidStateException("Unknown type '".get_class($type)."'!");
    }
    
    private function getReflectionNamedTypeDefinition(\ReflectionNamedType $type): array
    {
        if($type->isBuiltin()) {
            return $this->getScalarSchemaDefinition($type->getName());
        }

        if($type->getName() === Query::class) {
            $refName = $this->getQueryObjectDefinition();
            return ['$ref' => '#/components/schemas/'.$refName];
        }
        
        if(is_subclass_of($type->getName(), Entity::class)) {
            $refName = $this->getEntityObjectDefinition($type->getName());
            return ['$ref' => '#/components/schemas/'.$refName];
        }
        
        // FIXME: Generic object ?
        return [
            'type' => 'object'
        ];
    }
    
    private function getReflectionUnionTypeDefinition(\ReflectionUnionType $type): array
    {
        $types = [];
        foreach($type->getTypes() as $t) {
            
            // skip null from definitions like string|array|null
            if($t->getName() === 'null') {
                continue;
            }
            
            $types[] = $this->getReflectionNamedTypeDefinition($t);
        }
        return [
           'anyOf' => $types
        ];
    }
    
    private function getReflectionIntersectionTypeDefinition(\ReflectionIntersectionType $type): array
    {
        $types = [];
        foreach($type->getTypes() as $t) {
            $types[] = $this->getReflectionNamedTypeDefinition($t);
        }
        return [
           'oneOf' => $types
        ];
    }
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Response Attribute structure">
    
    private function getResponseContentDefinition(array|string $content): array
    {
        if(is_scalar($content)) {
            return $this->getScalarSchemaDefinition($content);
        }
        
        if(!is_array($content) || count($content) != 2) {
            throw new \Nette\InvalidStateException("Invalid 'Response' attribute definition");
        }
        
        // Structure
        if($content[0] === 'object') {
            $structProps = [];
            foreach($content[1] as $prop => $propDef) {
                $structProps[$prop] = $this->getResponseContentDefinition($propDef);
            }
            return [
                'type' => 'object',
                'properties' => $structProps
            ];
        }
        
        if($content[0] === 'array') {
            if(is_scalar($content[1])) {
                return [
                    'type' => 'array',
                    'items' => $this->getScalarSchemaDefinition($content[1])
                ];
            }
            return [
                'type' => 'array',
                'items' => $this->getResponseContentDefinition($content[1])
            ];
        }

        // FIXME: object
        return [
            'type' => 'object'
        ];
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Query object">

    private function getEntityObjectDefinition(string $entityClass): string
    {
        $entityPrefix = 'Entity_';
        
        $entity = EntityMetaStorage::describe($entityClass);
        $entityNameParts = explode('\\', $entity->name);
        $entityName = end($entityNameParts);
        
        if(isset($this->schemas[$entityPrefix.$entityName])) {
            return $entityPrefix.$entityName;
        }
        
        $this->schemas[$entityPrefix.$entityName] = 'pending...';
        
        $props = [];
        foreach($entity->properties as $prop) {
            
            // collection
            if(!empty($prop->related)) {
                $props[$prop->name] = [
                    'type' => 'array',
                    'items' => $this->getScalarSchemaDefinition($prop->type)
                ];
                continue;
            }
            
            // reference
            if(!empty($prop->ref)) {
                $props[$prop->name] = $this->getScalarSchemaDefinition($prop->type);
                continue;
            }
            
            // DateTime (date, time, datetime, timestamp
            if(class_exists($prop->type) && (is_subclass_of(ltrim($prop->type, '\\'), \DateTime::class) || ltrim($prop->type, '\\') === \DateTime::class)) {
                $props[$prop->name] = [
                    'type' => 'string',
                    'format' => match($prop->nativeType ?? 'datetime') {
                        'time' => 'time',
                        'date' => 'date',
                        'timestamp' => 'date-time',
                        'datetime' => 'date-time',
                        default => 'date-time'
                    }
                ];
                continue;
            }
            
            $props[$prop->name] = $this->getScalarSchemaDefinition($prop->type);
        }
        
        $this->schemas[$entityPrefix.$entityName] = [
            'type' => 'object',
            'properties' => $props
        ];
        
        return $entityPrefix.$entityName;
    }

    // </editor-fold>
    

    // <editor-fold defaultstate="collapsed" desc="Query object">

    
    private function getQueryObjectDefinition(): string
    {
        $queryPrefix = 'API_';
        
        if(isset($this->schemas[$queryPrefix.'Query'])) {
            return $queryPrefix.'Query';
        }
        
        $this->schemas[$queryPrefix.'Query_Filter'] = [
            'type' => 'object',
            'properties' => [
                'field' => [
                    'type' => 'string'
                ],
                'operator' => [
                    'type' => 'string'
                ],
                'value' => [
                    'oneOf' => [
                        ['type' => 'array'],
                        ['type' => 'object'],
                        ['type' => 'string'],
                        ['type' => 'number'],
                        ['type' => 'integer'],
                        ['type' => 'boolean'],
                    ]
                ]
            ]
        ];

        $this->schemas[$queryPrefix.'Query_Filter_Group'] = [
            'type' => 'object',
            'properties' => [
                'logic' => [
                    'type' => 'string',
                    'enum' => ['and','or'],
                ],
                'filters' => [
                    'type' => 'array',
                    'items' => [
                        'oneOf' => [
                            ['$ref' => '#/components/schemas/'.$queryPrefix.'Query_Filter'],
                            ['$ref' => '#/components/schemas/'.$queryPrefix.'Query_Filter_Group'],
                        ]
                    ],
                    'example' => [
                        ['field' => 'field', 'operator' => 'eq', 'value' => 'aaa']/*,[
                        'logic' => 'or',
                        'filters' => [[
                            'field' => 'someNumericField',
                            'operator' => '>=',
                            'value' => 5
                        ], [
                            'field' => 'someNumericField',
                            'operator' => '<=',
                            'value' => 10
                        ]]
                        ]*/
                    ]
                ]
            ]
        ];
        
        $this->schemas[$queryPrefix.'Query_Sort'] = [
            'type' => 'object',
            'properties' => [
                'field' => [
                    'type' => 'string'
                ],
                'dir' => [
                    'type' => 'string',
                    'enum' => ['asc', 'desc']
                ]
            ]
        ];
        
        $this->schemas[$queryPrefix.'Query_Criteria'] = [
            'type' => 'object',
            'properties' => [
                'page' => [
                    'type' => 'integer',
                    'example' => 1
                ],
                'pageSize' => [
                    'type' => 'integer',
                    'example' => 100
                ],
                'search' => [
                    'type' => 'string',
                    'example' => 'aaa'
                ],
                'filter' => [
                    '$ref' => '#/components/schemas/'.$queryPrefix.'Query_Filter_Group'
                ],
                'sort' => [
                    'type' => 'array',
                    'items' => [
                        '$ref' => '#/components/schemas/'.$queryPrefix.'Query_Sort'
                    ]
                ]
            ]
        ];
        
        $this->schemas[$queryPrefix.'Query'] = [
            'type' => 'object',
            'properties' => [
                'select' => [
                    'type' => 'string',
                    'example' => 'name,category(name)'
                ],
                'page' => [
                    'type' => 'integer',
                    'example' => 1
                ],
                'pageSize' => [
                    'type' => 'integer',
                    'example' => 100
                ],
                'search' => [
                    'type' => 'string',
                    'example' => 'aaa'
                ],
                "filter" => [
                    '$ref' => '#/components/schemas/'.$queryPrefix.'Query_Filter_Group'
                ],
                'sort' => [
                    'type' => 'array',
                    'items' => [
                        '$ref' => '#/components/schemas/'.$queryPrefix.'Query_Sort'
                    ]
                ],
                'criteria' => [
                    'type' => 'object',
                    'additionalProperties' => [
                        '$ref' => '#/components/schemas/'.$queryPrefix.'Query_Criteria'
                    ],
                    'example' => [
                        '@' => [
                          'page' => 1,
                          'pageSize' => 100
                        ],
                        'subCollection' => [
                            'page' => 1,
                            'pageSize' => 10
                        ]
                    ]
                ]
            ]
        ];
        
        return $queryPrefix.'Query';
    }
    

    // </editor-fold>

    
    
}
