<?php
namespace AbraD450\ApiServer\REST\Attributes;

use Nette;
use Nette\Utils\Json;

use Attribute;

/**
 * API Doc - Returns Attribute
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Response
{
    private int $code;
    
    private string|array|null $content;
    
    private ?string $contentType;
    
    private ?string $description;
        
    public function __construct(int $code, string|array|null $content = null, ?string $contentType = null, ?string $description = null)
    {
        $this->code = $code;
        $this->content = $content === null ? null : $this->validateContent($content);
        $this->contentType = $contentType;
        $this->description = $description;
    }
    
    public function getDefinition(): array
    {
        return [
            'code' => $this->code,
            'content' => $this->content,
            'contentType' => $this->contentType,
            'description' => $this->description ?? (string)$this->code
        ];
    }
    
    private function validateContent(string|array $content): string|array
    {
        if(is_array($content)) {
            if(count($content) != 2) {
                throw new Nette\InvalidStateException("Invalid 'Response' definition near: ".Json::encode($content).". Array must have exactly two items");
            }
            if(!in_array($content[0], ['object', 'array', '@pager'])) {
                throw new Nette\InvalidStateException("Invalid 'Response' definition. Expected 'object' or 'array' but '{$content[0]}' given near: ".Json::encode($content));
            }

            // objcet - substructure
            if($content[0] === 'object') {
                $subStruct = [];
                foreach($content[1] as $prop => $def) {
                    $subStruct[$prop] = $this->validateContent($def);
                }
                return [
                    $content[0],
                    $subStruct
                ];
            }
            
            // array of some type
            if($content[0] === 'array') {
                return [
                    $content[0],
                    $this->validateContent($content[1])
                ];
            }
            
            // @pager alias
            if($content[0] === '@pager') {
                return ['object', [
                    'data' => ['array', $this->validateContent($content[1])],
                    'total' => 'int',
                    'page' => 'int',
                    'pageSize' => 'int',
                    'from' => 'int',
                    'to' => 'int'
                ]];
            }

        }
        
        return $this->validateType($content);
    }
    
    private function validateType(string $type): string|array
    {
        $allowedBuiltin = [
            'int' => 'int',
            'integer' => 'int',
            'string' => 'string',
            'float' => 'float',
            'double' => 'float',
            'bool' => 'bool',
            'boolean' => 'bool',
        ];
        if(isset($allowedBuiltin[$type])) {
            return $allowedBuiltin[$type];
        }
        
        // alias?
        if(str_starts_with($type, '@')) {
            
            if($type === '@error') {
                return ['object', [
                    'error' => ['object', [
                        'code' => 'int',
                        'message' => 'string'
                    ]]
                ]];
            }
        }
        
        return $type;
    }
}