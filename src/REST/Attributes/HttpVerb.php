<?php
declare(strict_types=1);

namespace AbraD450\ApiServer\REST\Attributes;

use Attribute;

/**
 * Http GET Attribute
 */
#[Attribute(Attribute::TARGET_METHOD)]
abstract class HttpVerb
{
    /**
     * Gets HTTP verb based on Attribute Class Name
     * 
     * @return string
     */
    public function getHttpVerb(): string
    {
        $parts = explode('\\', get_called_class());
        return strtoupper(substr(end($parts), 4));
    }
}