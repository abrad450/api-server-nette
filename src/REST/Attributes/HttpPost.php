<?php
declare(strict_types=1);

namespace AbraD450\ApiServer\REST\Attributes;

use Attribute;

/**
 * Http GET Attribute
 */
#[Attribute(Attribute::TARGET_METHOD)]
class HttpPost extends HttpVerb
{
    
}
