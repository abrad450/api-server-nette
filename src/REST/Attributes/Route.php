<?php
namespace AbraD450\ApiServer\REST\Attributes;

use AbraD450\ApiServer\REST\ParamChecker;

use Attribute;

/**
 * Http GET Attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Route
{
    private string $path;
    
    private array $params;
    
    private ?array $definition = null;
        
    public function __construct($path, $params = [])
    {
        $this->path = $path;
        $this->params = $params;
    }
    
    public function getPath()
    {
        return $this->path;
    }
    
    public function getSimplePath()
    {
        return preg_replace('/\{([\?])?([a-z0-9_\*\-]+)[^\}]*\}/si', '{$2}', $this->path);
    }
    
    public function getParams()
    {
        return $this->params;
    }
    
    public function getDefinition(): ?array
    {
        if($this->definition === null) {
            $this->generateDefinition();
        }
        return $this->definition;
    }
    
    private function generateDefinition()
    {
        $matched = preg_match_all('/\{([\?])?([a-z0-9_\*\-]+)(?:\:(.*)){0,1}\}/Usi', $this->path, $matches, PREG_SET_ORDER);
        if($matched === false) {
            return;
        }
        if(count($matches) <= 0) {
            $this->definition = ['s', trim($this->path, '/'), [], []];
            return;
        }
        
        $regex = $this->path;
        
        $paramChecks = [];
        $paramDefs = [];
        
        foreach($matches as $match) {
            $optional = $match[1] === '?' ? '?' : '';
            
            $paramDef = $this->parseParamDef($match[2], $match[3] ?? '');
            $paramDef['optional'] = $match[1] === '?' ? true : false;
            
            if(!empty($paramDef['checks'])) {
                $paramChecks[$match[2]] = $paramDef['checks'];
            }
            
            $paramDefs[$match[2]] = $paramDef;
            
            $regex = str_replace($match[0], "(?P<{$match[2]}>{$paramDef['regex']}){$optional}", $regex);
        }
        
        $this->definition = ['r', trim($regex, '/'), $paramChecks, $paramDefs];
    }
    
    private function parseParamDef(string $name, string $def)
    {
        $defs = explode(':', $def);
        
        // default regex is string (without slash) with no checks
        $result = [
            'name' => $name,
            'def' => $def,
            'regex' => '[^/]+',
            'checks' => [],
            'dataType' => null
        ];
        
        foreach($defs as $d) {
            
            // data types + regex
            
            if($d === 'alpha') {
                $result['regex'] = '[a-zA-Z]+';
                $result['dataType'] = $result['dataType'] ?? 'string';
                continue;
            }
            
            if($d === 'guid') {
                $result['regex'] = '[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}';
                $result['dataType'] = $result['dataType'] ?? 'string';
                continue;
            }            
            
            if($d === 'bool') {
                $result['regex'] = 'true|false|0|1';
                $result['dataType'] = $result['dataType'] ?? 'bool';
                continue;
            }            

            if($d === 'float' || $d === 'decimal' || $d === 'double') {
                $result['regex'] = '[\-\.0-9]+';
                $result['dataType'] = $result['dataType'] ?? 'float';
                continue;
            }
            
            if($d === 'int' || $d === 'long') {
                $result['regex'] = '[\-0-9]+';
                $result['dataType'] = $result['dataType'] ?? 'int';
                continue;
            }            

            if($d === 'regex' && isset($this->params[$name])) {
                $result['regex'] = $this->params[$name];
                $result['dataType'] = $result['dataType'] ?? 'string';
                continue;
            }

            // catchall (usually the last parameter can catch all including slashes)
            if($def === 'all') {
                $result['regex'] = '.*';
                $result['dataType'] = $result['dataType'] ?? 'string';
                continue;
            }
            
            // checks
            $checkDef = ParamChecker::parse($d);
            if($checkDef !== null) {
                $result['checks'][] = $checkDef;
                continue;
            }
        }
        
        // Default = string
        $result['dataType'] = $result['dataType'] ?? 'string';
        
        return $result;
    }
}