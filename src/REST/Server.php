<?php

namespace AbraD450\ApiServer\REST;

use Nette\Utils\Strings;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

use Tracy\Debugger;

use AbraD450\ApiServer;
use AbraD450\ApiServer\InvalidParamsException;

use AbraD450\ApiServer\Common\Attributes\NonApi;
use AbraD450\ApiServer\Common\Attributes\Authorize;
use AbraD450\ApiServer\Common\Attributes\AllowAnonymous;

/**
 * REST API Server
 * 
 * @property-read array $config Configuration
 */
class Server extends ApiServer\BaseServer
{
    
    /**
     * Process
     */
    public function process()
    {
        $httpMethod = strtoupper($this->request->getMethod());
        $base = $this->request->getUrl()->getBasePath();
        $url = $this->request->getUrl()->getPath();
        $link = trim(Strings::substring($url, Strings::length($base)), '/');
        $endPoint = NULL;
        $updatedLink = NULL;

        $found = FALSE;
        foreach($this->config as $key => $dummy) {
            if(substr($link, 0, strlen($key) + 1) === $key.'/') {
                $updatedLink = substr($link, strlen($key) + 1);
                $endPoint = $key;
                $found = TRUE;
            }
        }
        if($found === FALSE) {
            return;
        }
        
        $apiExceptionHandler = function(\Throwable $e) {
            // silently log exception
            Debugger::log($e);
            // and send valid JSON to the output
            $error = $this->buildErrorCodeMessage(
                    ApiException::E_INTERNAL_ERROR, 
                    ApiException::E_INTERNAL_ERROR_TEXT,
                    $e->getMessage()
                );
            $httpCode = $e instanceof ApiException ? $e->getHttpCode() : 500;
            $this->sendResponse($error, $httpCode); 
        };
        
        set_exception_handler($apiExceptionHandler);
        
        try {
            if($httpMethod === 'OPTIONS') {
                $this->processOptions($endPoint);
            }
            else {
                $this->processApi($endPoint, $httpMethod, $updatedLink);
            }
        }
        catch(ApiException $e) {
            $error = $this->buildError($e);
            $this->sendResponse($error, $e->getHttpCode());
        }
    }

    
    /**
     * Process API on endpoint
     * 
     * @param string $endPoint End Point
     * @param string $httpMethod HTTP method in uppercase
     * @param string $url
     */
    protected function processApi(string $endPoint, string $httpMethod, string $url)
    {
        $this->sendAccessControlHeaders($endPoint);
        $config = &$this->config[$endPoint];
        $code = NULL;
        try {
            $response = $this->processRequest($config, $httpMethod, $url);
        }
        catch(ApiServer\AbortException $e) {
            exit;
        }
        catch(ApiException $e) {
            $response = $this->buildError($e);
            $code = $e->getHttpCode();
        }
        $this->sendResponse($response, $code);
    }
    
    /**
     * Process single request
     * 
     * @param array $config API configuration
     * @param array $request
     * @param string $httpMethod HTTP method in uppercase
     * @param string $url
     * @return mixed|NULL Response structure
     */
    protected function processRequest(array &$config, string $httpMethod, string $url)
    {
        $contentType = $this->request->getHeader('Content-Type') ?? 'application/json';
        
        // detect handler for this request
        $nsArray = is_array($config['namespace']) ? $config['namespace'] : [$config['namespace']];
        $handler = $this->detectHandler($nsArray, $url, $httpMethod);

        if(empty($handler)) {
            throw new ApiException(ApiException::E_METHOD_NOT_FOUND,
                "Method not found.",
                "ApiController and/or method was not found for URL: {$url}."
            );
        }
        
        $service = $this->dic->getService($handler['service']);
        /* @var $service ApiServer\ApiController */
        
        $serviceType = $this->dic->getServiceType($handler['service']);
        $serviceName = $handler['service'];
        
        $controller = $serviceType;
        $finalMethod = $handler['method'];
        
        $queryParams = $this->request->getQuery();
        $handlerParams = $handler['params'];
        
        // JSON raw body
        $rawBody = NULL;
        if($contentType == 'application/json' && in_array($httpMethod, ['POST', 'PUT', 'PATCH'])) {
            try {
                $requestRawBody = $this->request->getRawBody();
                $jsonForceArray = $config['jsonForceArray'] ?? true;
                if(strlen($requestRawBody ?? '') > 0) {
                    $rawBody = Json::decode($requestRawBody, $jsonForceArray ? Json::FORCE_ARRAY : 0);
                }
            }
            catch(JsonException $jsonEx) {
                throw new ApiException(ApiException::E_PARSE_ERROR, previous: $jsonEx);
            }
        }
        
        $allParameters = $handlerParams + $queryParams + (array)($rawBody ?? []);
        
        $this->dic->callInjects($service);
        
        $service->setParameters(
                $controller,
                $serviceName,
                $finalMethod,
                $allParameters,
                $rawBody
            );
        
        // Build parameters array
        try {
            $paramArr = $this->buildParameters($service, $finalMethod, $allParameters);
        }
        catch(InvalidParamsException $e) {
            throw new ApiException(ApiException::E_INVALID_PARAMS, $e->getMessage(), previous: $e);
        }
        
        // Call API function and grab the output
        if(!$service->isStartupCalled()) {
            $service->startup();
        }
        
        if(!$service->isStartupCalled()) {
            throw new ApiException(ApiException::E_INTERNAL_ERROR,
                NULL,
                "Service '{$service}' or its descendants doesn't call parent::startup()."
            );
        }

        // check authorization
        $this->checkAuthorization($handler['authorize']);

        return $service->{$finalMethod}(...$paramArr);
    }
    
    /**
     * Checks authorization
     * 
     * @param bool|array $authorize
     * @return void
     * @throws ApiException
     */
    protected function checkAuthorization(bool|array $authorize): void
    {
        if($authorize === false) {
            return;
        }
                
        if(is_array($authorize) && is_array($authorize['roles']) && $this->user->roles) {
            $intersectRoles = array_intersect($authorize['roles'], $this->user->roles);
            // User is not member of all requested roles
            if(count($intersectRoles) !== count($authorize['roles'])) {
                throw new ApiException(
                        code: 403,
                        message: 'Forbidden',
                        internal: 'Required all of the roles: '.json_encode($authorize['roles']).'. Roles of the user: '.json_encode($this->user->roles),
                        httpCode: 403
                    );
            }
            // user has all required roles
            return;
        }
        
        if(!$this->user->isLoggedIn()) {
            throw new ApiException(
                    code: 401,
                    message: 'Unauthorized',
                    internal: 'User is not authenticated, but authentication is required',
                    httpCode: 401
                );
        }
    }
    
    /**
     * Detects handler for the given URL
     * 
     * @param array $namespaces Namespace array
     * @param string $url URL
     * @param string $httpMethod HTTP verb in uppercase
     * @return array|null
     */
    protected function detectHandler(array $namespaces, string $url, string $httpMethod): ?array
    {
        $rules = $this->getHandlersCache($namespaces);

        foreach($rules as $rule) {
            
            $cParams = [];
            $methodUrl = $url;
            
            // MATCH CONTROLLER
            $cMatched = false;
            
            // starts with (s)
            if($rule['type'] === 's' && str_starts_with($url, $rule['match'])) {
                $methodUrl = Strings::substring($methodUrl, Strings::length($rule['match']));
                $cMatched = true;
            }
            elseif($rule['type'] === 'r') {
                // reguler expression match (r)
                $m = str_replace('#', '\\#', $rule['match']);
                $matches = null;
                if(preg_match("#^{$m}#si", $url, $matches)) {
                    $cParams = array_filter($matches, fn($key) => !is_int($key), ARRAY_FILTER_USE_KEY);
                    // run also checks
                    if(ParamChecker::checkParams($cParams, $rule['checks'] ?? [])) {
                        $methodUrl = Strings::substring($methodUrl, Strings::length($matches[0]));
                        $cMatched = true;
                    }
                }
            }
            
            if(!$cMatched) {
                continue;
            }
            
            
            // Controller matched - find method within this controller
            $methodUrlTrimed = ltrim($methodUrl, '/');
            foreach($rule['methods'] as $mRule) {
                
                // starts with (s)
                if($mRule['type'] === 's' && 
                    // match must be equal to rest of the URL
                    ($methodUrlTrimed === $mRule['match']) &&
                    (empty($mRule['verbs']) || isset($mRule['verbs'][$httpMethod]))
                ) {
                    return [
                        'service' => $rule['service'],
                        'method' => $mRule['method'],
                        'params' => $cParams,
                        'authorize' => $this->buildHandlerAuthorize($rule, $mRule)
                    ];
                }
                elseif($mRule['type'] === 'r') {
                    // reguler expression match (r)
                    $m = str_replace('#', '\\#', $mRule['match'].'/?');
                    $mMatches = null;
                    if(preg_match("#^{$m}$#Usi", $methodUrlTrimed.'/', $mMatches) &&
                        (empty($mRule['verbs']) || isset($mRule['verbs'][$httpMethod]))
                    ) {
                        $mParams = array_filter($mMatches, fn($key) => !is_int($key), ARRAY_FILTER_USE_KEY);
                        // run also checks
                        if(ParamChecker::checkParams($mParams, $mRule['checks'] ?? [])) {
                            return [
                                'service' => $rule['service'],
                                'method' => $mRule['method'],
                                'params' => array_merge($cParams, $mParams),
                                'authorize' => $this->buildHandlerAuthorize($rule, $mRule)
                            ];
                        }                        
                    }
                }
            }

        }

        return null;
    }
    
    /**
     * Get handlers cache
     * 
     * @param array $namespaces
     * @return array
     */
    public function getHandlersCache(array $namespaces): array
    {
        $cacheKey = 'ns.'.implode('|', $namespaces);
        $cached = $this->cache->load($cacheKey);
        if(Debugger::$productionMode && $cached !== null) {
            return $cached;
        }
            
        $apiControllers = $this->dic->findAutowired(ApiServer\ApiController::class);

        $rules = [];

        foreach($apiControllers as $apic) {

            $c = $this->dic->getService($apic);

            // Another namespace
            $refClass = new \ReflectionClass($c);
            $classNamespace = $refClass->getNamespaceName();

            // test if namespace of the class is in the given array
            $invalidNamespace = true;
            foreach($namespaces as $ns) {
                if(str_starts_with($classNamespace, $ns)) {
                    $invalidNamespace = false;
                    break;
                }
            }
            if($invalidNamespace) {
                continue;
            }

            // Class match
            $rule = $this->getControllerMatch($refClass);
            $rule['service'] = $apic;
            $rule['serviceType'] = $class = $refClass->getName();
            
            // Controller's Authorize
            $cAuthorize = $refClass->getAttributes(Authorize::class);
            $cAuthorizeDef = null;
            if(!empty($cAuthorize)) {
                $cAuthorizeInstance = reset($cAuthorize)->newInstance();
                /* @var $cAuthorizeInstance Authorize */
                $cAuthorizeDef = [
                    'on' => true,
                    'roles' => $cAuthorizeInstance->getRoles()
                ];
            }
            $rule['authorize'] = $cAuthorizeDef;
            
            // Methods
            $methods = $refClass->getMethods(\ReflectionMethod::IS_PUBLIC);
            $rule['methods'] = [];
            foreach($methods as $method) {
                /* @var $method \ReflectionMethod */

                // Skip ApiController's methods
                // Skip NonApi declared methods
                // Skip constructor / destructor and startup
                // ALL other PUBLIC methods will be callable via API !
                if($method->class === ApiServer\ApiController::class ||
                        str_starts_with($method->name, '__') ||
                        $method->name === 'startup' ||
                        count($method->getAttributes(NonApi::class)) > 0
                        ) {
                    continue;
                }

                $m = $this->getControllerMethodMatch($method);
                $m['method'] = $method->name;

                // HTTP verbs
                $verbsAttrs = $method->getAttributes(Attributes\HttpVerb::class, \ReflectionAttribute::IS_INSTANCEOF);
                $verbs = [];
                foreach($verbsAttrs as $va) {
                    /* @var $va \ReflectionAttribute */
                    $verbs[$va->newInstance()->getHttpVerb()] = true;
                }
                $m['verbs'] = $verbs;
                
                // Methods's Authorize
                $mAuthorize = $method->getAttributes(Authorize::class);
                $mAuthorizeDef = null;
                if(!empty($mAuthorize)) {
                    $mAuthorizeInstance = reset($mAuthorize)->newInstance();
                    /* @var $mAuthorizeInstance Authorize */
                    $mAuthorizeDef = [
                        'on' => true,
                        'roles' => $mAuthorizeInstance->getRoles()
                    ];
                }
                // Can be opt-outed with AllowAnonymous
                $mAllowAnonymous = $method->getAttributes(AllowAnonymous::class);
                if(!empty($mAllowAnonymous)) {
                    $mAuthorizeDef = [
                        'allowedAnonymous' => true
                    ];
                }
                
                $m['authorize'] = $mAuthorizeDef;

                $rule['methods'][] = $m;
            }
            
            // Add rule to result
            $rules[] = $rule;
        }

        $this->cache->save($cacheKey, $rules);

        return $rules;
    }

    /**
     * Get controller class match rule
     * 
     * @param \ReflectionClass $clsReflection
     * @return array ['type' => 's'|'r', 'match']
     */
    protected function getControllerMatch(\ReflectionClass $clsReflection): array
    {
        // Attribute Route
        $attrs = $clsReflection->getAttributes(Attributes\Route::class);
        if(count($attrs) > 0) {
            $attr = end($attrs);
            /* @var $attr \ReflectionAttribute */
            $at = $attr->newInstance();
            /* @var $at Attributes\Route */
            [$type, $match, $checks, $params] = $at->getDefinition();
            return ['type' => $type, 'match' => $match, 'checks' => $checks, 'params' => $params, 'path' => $at->getPath(), 'simplePath' => $at->getSimplePath()];
        }

        // Match according to a class name - without "Controller" suffix
        $shortName = $clsReflection->getShortName();
        $ctrName = preg_replace_callback(
            '/[A-Z]{1}/U',
            fn($matches) => '-'.Strings::lower($matches[0]),
            lcfirst(str_ends_with($shortName, 'Controller') ? Strings::substring($shortName, 0, -10) : $shortName)
        );
        return ['type' => 's', 'match' => $ctrName, 'checks' => [], 'params' => [], 'path' => $ctrName, 'simplePath' => $ctrName];
    }

    /**
     * Get controller's method match rule
     * 
     * @param \ReflectionMethod $refMethod
     * @return array ['type' => 's'|'r', 'match']
     */
    protected function getControllerMethodMatch(\ReflectionMethod $refMethod): array
    {
        // Attribute Route
        $attrs = $refMethod->getAttributes(Attributes\Route::class);
        if(count($attrs) > 0) {
            $attr = end($attrs);
            /* @var $attr \ReflectionAttribute */
            $at = $attr->newInstance();
            /* @var $at Attributes\Route */
            [$type, $match, $checks, $params] = $at->getDefinition();
            return ['type' => $type, 'match' => $match, 'checks' => $checks, 'params' => $params, 'path' => $at->getPath(), 'simplePath' => $at->getSimplePath()];
        }

        // Match according to a method name
        $shortName = $refMethod->getShortName();
        $methodName = preg_replace_callback(
            '/[A-Z]{1}/U',
            fn($matches) => '-'.Strings::lower($matches[0]),
            lcfirst($shortName)
        );
        
        return ['type' => 's', 'match' => $methodName, 'checks' => [], 'params' => [], 'path' => $methodName, 'simplePath' => $methodName];
    }
    
     
    // --------------------------------------------------------------------------
    
    
    /**
     * Build error from Exception
     * 
     * @param ApiException $e
     * @param int|NULL $id
     */
    protected function buildError(ApiException $e): array
    {
        return [
            'error' => [
                'code' => $e->getCode(),
                'message' => trim($e->getMessage().
                        // If Nette Debug mode is ON, show also internal message with detailed error description
                        (Debugger::$productionMode ? '' : ' '.$e->getInternalMessage())
                    )
            ]
        ];
    }    
    
    /**
     * Build error structure
     * 
     * @param int $code
     * @param string $message
     * @param string $internalMessage
     * @return array Structure
     */
    protected function buildErrorCodeMessage(int $code, string $message, string $internalMessage = NULL)
    {
        return [
            'error' => [
                'code' => $code,
                'message' => trim(
                        $message.
                        // If Nette Debug mode is ON, show also internal message with detailed error description
                        (Debugger::$productionMode ? '' : ' '.($internalMessage ?? ''))
                    )
            ]
        ];
    }
    
    // -------------------------------------------------------------------------
    
    
    /**
     * Send structure to the standard output and exit
     * 
     * @param array|\stdClass $response Structure
     * @param int $code
     */
    protected function sendResponse($response, $code = NULL)
    {
        if($code) {
            $this->response->setCode($code);
        }
        
        // Response object
        if($response instanceof \Nette\Application\Response) {
            $response->send($this->request, $this->response);
            exit;
        }
        
        // JSON output
        $this->response->setHeader('Content-Type', 'application/json');
        if($response !== NULL) {
            print Json::encode($response);
        }
        exit;
    }
    
}