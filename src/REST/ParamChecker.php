<?php

namespace AbraD450\ApiServer\REST;

use Nette\Utils\Strings;

/**
 * Param Checker
 */
class ParamChecker
{

    /**
     * Check parameters
     * 
     * @param array $params ['paramName' => PARAM_VALUE]
     * @param array $checkers ['paramName' => [check, check, check]
     * @return bool
     */
    public static function checkParams(array $params, array $checkers)
    {
        foreach($params as $param => $value) {
            if(!isset($checkers[$param])) {
                continue;
            }
            if(!self::checkRules($value, $checkers[$param])) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Check rules
     * 
     * @param mixed $value Value
     * @param array $rules Array of checks (rules)
     * @return bool
     */
    public static function checkRules(mixed $value, array $rules = []): bool
    {
        foreach($rules as $rule) {
            if(!self::checkRule($value, $rule)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Check one rule
     * 
     * @param mixed $value Value
     * @param array $rule Rule definition
     * @return bool
     */
    public static function checkRule(mixed $value, array $rule): bool
    {
        $methodName = "check".ucfirst($rule['name']);
        return self::$methodName($value, $rule['value']);
    }
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Rules Checks">


    public static function checkLength(mixed $value, int|array $length): bool
    {
        $len = Strings::length($value);
        if(is_array($length)) {
            return $len >= $length[0] && $len <= $length[1];
        }
        return $len == $length;
    }
    
    public static function checkMinlength(mixed $value, int $minLength): bool
    {
        return Strings::length($value) >= $minLength;
    }
    
    public static function checkMaxlength(mixed $value, int $maxLength): bool
    {
        return Strings::length($value) <= $maxLength;
    }
    
    public static function checkMin(mixed $value, int|float $min): bool
    {
        return $value >= $min;
    }
    
    public static function checkMax(mixed $value, int|float $max): bool
    {
        return $value <= $max;
    }
    
    public static function checkRange(mixed $value, array $range): bool
    {
        return $value >= $range[0] && $value <= $range[1];
    }
    
    
    // </editor-fold>
    
    
    
    
    /**
     * Rules parser
     * 
     * Returns array with 'name' and 'value'
     * Each parsed definition MUST HAVE corresponding check{Name} method accepting 'value' key
     * 
     * @param string $definition
     * @return array|null
     */
    public static function parse(string $definition): ?array
    {
        $matches = NULL;
        if(preg_match('/^range\(([\-\.0-9]+),([\-\.0-9]+)\)$/Ui', $definition, $matches)) {
            return [
                'name' => 'range',
                'value' => [
                    strpos($matches[1], '.') === false ? intval($matches[1]) : floatval($matches[1]),
                    strpos($matches[2], '.') === false ? intval($matches[2]) : floatval($matches[2])
                ]
            ];
        }

        if(preg_match('/^min\(([\-\.0-9]+)\)$/Ui', $definition, $matches)) {
            return [
                'name' => 'min',
                'value' => strpos($matches[1], '.') === false ? intval($matches[1]) : floatval($matches[1]),
            ];
        }

        if(preg_match('/^max\(([\-\.0-9]+)\)$/Ui', $definition, $matches)) {
            return [
                'name' => 'max',
                'value' => strpos($matches[1], '.') === false ? intval($matches[1]) : floatval($matches[1]),
            ];
        }

        if(preg_match('/^length\(([0-9]+)(?:,([0-9]+))?\)$/Ui', $definition, $matches)) {
            if(count($matches) == 2) {
                // length(X)
                return [
                    'name' => 'length',
                    'value' => intval($matches[1])
                ];
            }
            else {
                // length(X,Y)
                return [
                    'name' => 'length',
                    'value' => [
                        intval($matches[1]),
                        intval($matches[2])
                    ]
                ];
            }
        }

        if(preg_match('/^minlength\(([0-9]+)\)$/Ui', $definition, $matches)) {
            return [
                'name' => 'minlength',
                'value' => intval($matches[1])
            ];
        }

        if(preg_match('/^maxlength\(([0-9]+)\)$/Ui', $definition, $matches)) {
            return [
                'name' => 'maxlength',
                'value' => intval($matches[1])
            ];
        }
        
        return null;
    }
    
}
