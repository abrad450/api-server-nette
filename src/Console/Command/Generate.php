<?php

namespace AbraD450\ApiServer\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use AbraD450\ApiServer\REST\Documentation\OpenAPIGenerator;

#[AsCommand(name: 'generate')]
class Generate extends Command
{
    
    protected static $defaultName = 'generate';
    
    private OpenAPIGenerator $openApiGenerator;

    public function __construct(OpenAPIGenerator $openApiGenerator, string $name = null)
    {
        parent::__construct($name);
        $this->openApiGenerator = $openApiGenerator;
    }
    
    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->addArgument('endpoint');
        $this->setDescription('Generate OpenAPI Documentation')
            ->setHelp(
                <<<EOT
The <info>generate</info> command generates OpenAPI docs from your API Controllers
                    
<info>apiserver generate api/v1</info>
EOT
            );
    }

    /**
     * Rollback the migration.
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input Input
     * @param \Symfony\Component\Console\Output\OutputInterface $output Output
     * @return int integer 0 on success, or an error code.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $endpoint = $input->getArgument('endpoint');
        $output->writeln('Generating OpenAPI docs:');
        $generated = $this->openApiGenerator->generate($endpoint);
        foreach($generated as $endpoint => $cfg) {
            $path = realpath($cfg['file']);
            $output->writeln("- endpoint '<info>{$endpoint}</info>' => '<info>{$path}</info>'");
        }
        return 0;
    }
} 