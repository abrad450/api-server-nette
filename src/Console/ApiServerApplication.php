<?php

namespace AbraD450\ApiServer\Console;

use AbraD450\ApiServer\Console\Command\Generate;
use AbraD450\ApiServer\REST\Documentation\OpenAPIGenerator;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Phinx console application.
 *
 * @author Rob Morgan <robbym@gmail.com>
 */
class ApiServerApplication extends Application
{
    /**
     * Initialize the Phinx console application.
     */
    public function __construct(OpenAPIGenerator $openApiGenerator)
    {
        parent::__construct('<info>API Server</info>');

        $this->addCommands([
            new Generate($openApiGenerator),
        ]);
    }

}
