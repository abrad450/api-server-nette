<?php

namespace AbraD450\ApiServer\Common\Attributes;

use Attribute;

/**
 * Allow Anonymous for method of Authorized Controller
 */
#[Attribute(Attribute::TARGET_METHOD)]
class AllowAnonymous
{

}
