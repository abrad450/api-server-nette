<?php

namespace AbraD450\ApiServer\Common\Attributes;

use Attribute;

/**
 * Description of Authorize
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Authorize
{
    private array $roles;
           
    public function __construct(array|string $roles = [])
    {
        $this->roles = is_array($roles) ? $roles : [$roles];
    }
    
    public function getRoles(): array
    {
        return $this->roles;
    }
}
