<?php

namespace AbraD450\ApiServer\Common\Attributes;

use Attribute;

use Nette;
use Nette\Utils\Json;

/**
 * Parameter definition
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Parameter
{
    private string $name;
    
    private string|array|null $definition;
           
    public function __construct(string $name, string|array|null $definition = null)
    {
        $this->name = $name;
        $this->definition = $definition === null ? null : $this->validateContent($definition);
    }
    
    public function getName(): string
    {
        return $this->name;
    }
    
    public function getDefinition(): string|array|null
    {
        return $this->definition;
    }
    
    
    private function validateContent(string|array $content): string|array
    {
        if(is_array($content)) {
            if(count($content) != 2) {
                throw new Nette\InvalidStateException("Invalid 'Response' definition near: ".Json::encode($content).". Array must have exactly two items");
            }
            if(!in_array($content[0], ['object', 'array', '@pager'])) {
                throw new Nette\InvalidStateException("Invalid 'Response' definition. Expected 'object' or 'array' but '{$content[0]}' given near: ".Json::encode($content));
            }

            // objcet - substructure
            if($content[0] === 'object') {
                $subStruct = [];
                foreach($content[1] as $prop => $def) {
                    $subStruct[$prop] = $this->validateContent($def);
                }
                return [
                    $content[0],
                    $subStruct
                ];
            }
            
            // array of some type
            if($content[0] === 'array') {
                return [
                    $content[0],
                    $this->validateContent($content[1])
                ];
            }
            
            // @pager alias
            if($content[0] === '@pager') {
                return ['object', [
                    'data' => ['array', $this->validateContent($content[1])],
                    'total' => 'int',
                    'page' => 'int',
                    'pageSize' => 'int',
                    'from' => 'int',
                    'to' => 'int'
                ]];
            }

        }
        
        return $this->validateType($content);
    }
    
    private function validateType(string $type): string|array
    {
        $allowedBuiltin = [
            'int' => 'int',
            'integer' => 'int',
            'string' => 'string',
            'float' => 'float',
            'double' => 'float',
            'bool' => 'bool',
            'boolean' => 'bool',
        ];
        if(isset($allowedBuiltin[$type])) {
            return $allowedBuiltin[$type];
        }
        
        // alias?
        if(str_starts_with($type, '@')) {
            
            if($type === '@error') {
                return ['object', [
                    'error' => ['object', [
                        'code' => 'int',
                        'message' => 'string'
                    ]]
                ]];
            }
        }
        
        return $type;
    }    
    
}
