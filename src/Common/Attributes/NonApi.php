<?php
namespace AbraD450\ApiServer\Common\Attributes;

use Attribute;

/**
 * Attribute for declaring that this public method is not an API method
 */
#[Attribute(Attribute::TARGET_METHOD)]
class NonApi
{
}