<?php
namespace AbraD450\ApiServer\RPC\Attributes;

use Attribute;

/**
 * Attribute for declaring that this public method is not an API method
 * 
 * Use AbraD450\ApiServer\Common\Attributes\NonApi
 * 
 * @deprecated
 */
class NonApi extends \AbraD450\ApiServer\Common\Attributes\NonApi
{
}