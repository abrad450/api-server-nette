<?php
namespace AbraD450\ApiServer\RPC\Attributes;

use Attribute;

/**
 * Http GET Attribute
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Path
{
    private string $path;
    
    public function __construct($path)
    {
        $this->path = $path;
    }
    
    public function getPath()
    {
        return $this->path;
    }
    
}