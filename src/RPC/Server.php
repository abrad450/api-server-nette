<?php

namespace AbraD450\ApiServer\RPC;

use Nette;
use Nette\Utils\Strings;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

use Tracy\Debugger;

use AbraD450\ApiServer;
use AbraD450\ApiServer\InvalidParamsException;

use ApiServer\Common\Attributes\NonApi;
use AbraD450\ApiServer\Common\Attributes\Authorize;
use AbraD450\ApiServer\Common\Attributes\AllowAnonymous;

/**
 * RPC Server
 * 
 * @property-read array $config Configuration
 */
class Server extends ApiServer\BaseServer
{
    /** JSON RPC specification version */
    const JSON_RPC_VERSION = '2.0';
    
    // -------------------------------------------------------------------------

    /**
     * Process
     */
    public function process()
    {
        $catchMethods = ['post', 'options'];
        $method = Strings::lower($this->request->getMethod());
        if(!in_array($method, $catchMethods)) {
            return;
        }
        $base = $this->request->getUrl()->getBasePath();
        $url = $this->request->getUrl()->getPath();
        $link = trim(Strings::substring($url, Strings::length($base)), '/');
        if(!isset($this->config[$link])) {
            return;
        }
        
        $apiExceptionHandler = function(\Throwable $e) {
            // silently log exception
            Debugger::log($e);
            // and send valid JSON to the output
            $error = $this->buildErrorCodeMessage(
                    ApiException::E_INTERNAL_ERROR, 
                    ApiException::E_INTERNAL_ERROR_TEXT,
                    $e->getMessage()
                );
            $this->sendResponse($error); 
        };
        
//        $apiErrorHandler = function($errno, $errstr, $errfile, $errline) {
//            // Ignore deprecated warnings
//            if($e === E_DEPRECATED || $e === E_USER_DEPRECATED) {
//                return;
//            }
//            
//            // silently log error - exception
//            \Tracy\Debugger::log(new \ErrorException($errstr, NULL, $errno, $errfile, $errline));
//
//            // and send valid JSON to the output
//            $error = $this->buildErrorCodeMessage(
//                    ApiException::E_INTERNAL_ERROR, 
//                    ApiException::E_INTERNAL_ERROR_TEXT,
//                    $errstr
//                );
//            $this->sendResponse($error); 
//        };
//        set_error_handler($apiErrorHandler);

        set_exception_handler($apiExceptionHandler);
        
        try {
            if($method === 'options') {
                $this->processOptions($link);
            }
            else {
                $this->processApi($link);
            }
        }
        catch(ApiException $e) {
            $error = $this->buildError($e);
            $this->sendResponse($error); 
        }
    }
    
    /**
     * Send Access-Control-* headers based on endpoint configuration
     * 
     * @param string $endPoint
     * @return void
     */
    protected function sendAccessControlHeaders(string $endPoint): void
    {
        $config = &$this->config[$endPoint];
        if(empty($config['accessControl'])) {
            return;
        }
        
        // Get ORIGIN
        $origin = $this->request->getHeader('origin');
        if(empty($origin)) {
            return;
        }

        $ac = &$config['accessControl'];

        if(!empty($ac['allowedOrigins']) && is_array($ac['allowedOrigins'])) {
            $origin = in_array($origin, $ac['allowedOrigins']) || in_array('*', $ac['allowedOrigins']) ? $origin : '?';
        }
        
        $this->response->addHeader('Access-Control-Allow-Origin', $origin);
        $this->response->addHeader('Access-Control-Allow-Credentials',
                (empty($ac['allowCredentials']) ? 'false' : 'true'));
        $this->response->addHeader('Access-Control-Allow-Methods', 
                (empty($ac['allowMethods']) ? 'POST, OPTIONS' : implode(', ', $ac['allowMethods'])));
        $this->response->addHeader('Access-Control-Allow-Headers',
                (empty($ac['allowHeaders']) ? 'origin, content-type, accept, x-tracy-ajax' : implode(', ', $ac['allowHeaders'])));
        $this->response->addHeader('Access-Control-Expose-Headers',
                (empty($ac['exposeHeaders']) ? 'origin, location, content-type, accept, x-tracy-ajax' : implode(', ', $ac['exposeHeaders'])));
        $this->response->addHeader('Access-Control-Max-Age',
                (empty($ac['maxAge']) ? "1728000" : (string)$ac['maxAge']));
    }
    
    /**
     * Process OPTIONS request
     * 
     * @param string $endPoint
     */
    protected function processOptions(string $endPoint): void
    {
        $this->sendAccessControlHeaders($endPoint);
        $this->sendResponse(null);
    }    
    
    /**
     * Process API on endpoint
     * 
     * @param string $endPoint End Point
     */
    protected function processApi(string $endPoint)
    {
        $this->sendAccessControlHeaders($endPoint);
        $body = $this->request->getRawBody();
        try {
            $request = Json::decode($body, Json::FORCE_ARRAY);
        }
        catch(JsonException $ex) {
            throw new ApiException(ApiException::E_PARSE_ERROR, NULL, $ex);
        }
        
        if(empty($request)) {
            throw new ApiException(ApiException::E_INVALID_REQUEST);
        }

        $config = &$this->config[$endPoint];
        
        // Batch request
        if(is_numeric(key($request))) {
            $response = NULL;
            $responses = [];
            foreach($request as $one) {
                $isNotification = !isset($one['id']) || !is_scalar($one['id']) || trim($one['id']) === '';
                $id = $isNotification ? NULL : $one['id'];
                try {
                    $resp = $this->processRequest($config, $one, $isNotification);
                    // Was it notification ?
                    if(!$isNotification) {
                        $responses[] = $resp;
                    }
                }
                catch(ApiServer\AbortException) {
                    $responses[] = null;
                }
                catch(ApiException $e) {
                    $responses[] = $this->buildError($e, $id);
                }
            }
            if(!empty($responses)) {
                $response = $responses;
            }
            $this->sendResponse($response);
            return;
        }

        
        // Single request
        $response = NULL;
        $isNotification = !isset($request['id']) || !is_scalar($request['id']) || trim($request['id']) === '';
        $id = $isNotification ? NULL : $request['id'];
        try {
            $resp = $this->processRequest($config, $request, $isNotification);
            if(!$isNotification) {
                $response = $resp;
            }
        }
        catch(ApiServer\AbortException) {
            $response = null;
        }        
        catch(ApiException $e) {
            $response = $this->buildError($e, $id);
        }
        $this->sendResponse($response);
    }
    
    /**
     * Process single request
     * 
     * @param array $config API configuration
     * @param \stdClass $request
     * @param bool $isNotification Is this request notification ?
     * @return mixed|NULL Response structure
     */
    protected function processRequest(array &$config, $request, $isNotification)
    {
        $id = $isNotification ? NULL : $request['id'];
        
        // Check request syntax
        if(!$this->checkRequestSyntax($request)) {
            throw new ApiException(ApiException::E_INVALID_REQUEST);
        }
        
        // 
        $nsArray = is_array($config['namespace']) ? $config['namespace'] : [$config['namespace']];
        $handler = $this->detectHandler($nsArray, $request['method']);

        if(empty($handler)) {
            throw new ApiException(ApiException::E_METHOD_NOT_FOUND,
                "Method not found.",
                "ApiController and/or Controller's method was not found for method call: {$request['method']}."
            );
        }        
        
        $service = $this->dic->getService($handler['service']);
        /* @var $service ApiServer\ApiController */
        
        $serviceType = $this->dic->getServiceType($handler['service']);
        $serviceName = $handler['service'];
        
        $controller = $serviceType;
        $finalMethod = $handler['method'];
        
        $this->dic->callInjects($service);
        
        $service->setParameters(
                $controller,
                $serviceName,
                $finalMethod
            );        
        
        // Build parameters array
        try {
            $paramArr = $this->buildParameters($service, $finalMethod, (isset($request['params']) ? $request['params'] : []));
        }
        catch(InvalidParamsException $e) {
            throw new ApiException(ApiException::E_INVALID_PARAMS, $e->getMessage(), previous: $e);
        }

        // Call API function and grab the output
        if(!$service->isStartupCalled()) {
            $service->startup();
        }

        if(!$service->isStartupCalled()) {
            throw new ApiException(ApiException::E_INTERNAL_ERROR,
                NULL,
                "Service '{$service}' or its descendants doesn't call parent::startup()."
            );
        }
        
        // check authorization
        $this->checkAuthorization($handler['authorize']);

        $result = $service->{$finalMethod}(...$paramArr);

        if(!$isNotification) {
            return $this->buildResponse($result, $id);
        }
        return NULL;
    }
    
    /**
     * Checks authorization
     * 
     * @param bool|array $authorize
     * @return void
     * @throws ApiException
     */
    protected function checkAuthorization(bool|array $authorize): void
    {
        if($authorize === false) {
            return;
        }
                
        if(is_array($authorize) && is_array($authorize['roles']) && $this->user->roles) {
            $intersectRoles = array_intersect($authorize['roles'], $this->user->roles);
            // User is not member of all requested roles
            if(count($intersectRoles) !== count($authorize['roles'])) {
                throw new ApiException(
                        code: 403,
                        message: 'Forbidden',
                        internal: 'Required all of the roles: '.json_encode($authorize['roles']).'. Roles of the user: '.json_encode($this->user->roles)
                    );
            }
            // user has all required roles
            return;
        }
        
        if(!$this->user->isLoggedIn()) {
            throw new ApiException(
                    code: 401,
                    message: 'Unauthorized',
                    internal: 'User is not authenticated, but authentication is required'
                );
        }
    }    
    
    
    
    
    
    
    /**
     * Detects handler for the given URL
     * 
     * @param array $namespaces Namespace array
     * @param string $method Mathod path
     * @return array|null
     */
    protected function detectHandler(array $namespaces, string $method): ?array
    {
        $rules = $this->getHandlersCache($namespaces);
        
        
        foreach($rules as $rule) {
            
            if(!str_starts_with($method, $rule['match'])) {
                continue;
            }
            
            $remaining = Strings::substring($method, Strings::length($rule['match']));
            $remainingTr = ltrim($remaining, './');

            foreach($rule['methods'] as $mRule) {
                
                if($remainingTr != $mRule['match']) {
                    continue;
                }
                
                return [
                    'service' => $rule['service'],
                    'method' => $mRule['method'],
                    'authorize' => $this->buildHandlerAuthorize($rule, $mRule)
                ];
            }                

        }
        
        return null;
    }
    
    /**
     * Get handlers cache
     * 
     * @param array $namespaces
     * @return array
     */
    protected function getHandlersCache(array $namespaces): array
    {
        $cacheKey = 'ns.'.implode('|', $namespaces);
        $cached = $this->cache->load($cacheKey);
        
        if(Debugger::$productionMode && $cached !== null) {
            return $cached;
        }

        $apiControllers = $this->dic->findAutowired(ApiServer\ApiController::class);

        $rules = [];

        foreach($apiControllers as $apic) {

            $c = $this->dic->getService($apic);

            // Another namespace
            $refClass = new \ReflectionClass($c);
            $classNamespace = $refClass->getNamespaceName();

            // test if namespace of the class is in the given array
            $invalidNamespace = true;
            foreach($namespaces as $ns) {
                if(str_starts_with($classNamespace, $ns)) {
                    $invalidNamespace = false;
                    break;
                }
            }
            if($invalidNamespace) {
                continue;
            }

            $class = $refClass->getName();
            // Class match
            $rule = [
                'match' => $this->getControllerMatch($refClass),
                'service' => $apic,
                'serviceType' => $class
            ];
            
            // Controller's Authorize
            $cAuthorize = $refClass->getAttributes(Authorize::class);
            $cAuthorizeDef = null;
            if(!empty($cAuthorize)) {
                $cAuthorizeInstance = reset($cAuthorize)->newInstance();
                /* @var $cAuthorizeInstance Authorize */
                $cAuthorizeDef = [
                    'on' => true,
                    'roles' => $cAuthorizeInstance->getRoles()
                ];
            }
            $rule['authorize'] = $cAuthorizeDef;           
            
            // Methods
            $methods = $refClass->getMethods(\ReflectionMethod::IS_PUBLIC);
            $rule['methods'] = [];
            foreach($methods as $method) {
                /* @var $method \ReflectionMethod */

                // Skip ApiController's methods
                // Skip NonApi declared methods
                // Skip constructor / destructor and startup
                // ALL other PUBLIC methods will be callable via API !
                if($method->class === ApiServer\ApiController::class ||
                        str_starts_with($method->name, '__') ||
                        $method->name === 'startup' ||
                        count($method->getAttributes(NonApi::class)) > 0
                        ) {
                    continue;
                }

                $m = [
                    'match' => $this->getControllerMethodMatch($method),
                    'method' => $method->name
                ];
                
                // Methods's Authorize
                $mAuthorize = $method->getAttributes(Authorize::class);
                $mAuthorizeDef = null;
                if(!empty($mAuthorize)) {
                    $mAuthorizeInstance = reset($mAuthorize)->newInstance();
                    /* @var $mAuthorizeInstance Authorize */
                    $mAuthorizeDef = [
                        'on' => true,
                        'roles' => $mAuthorizeInstance->getRoles()
                    ];
                }
                // Can be opt-outed with AllowAnonymous
                $mAllowAnonymous = $method->getAttributes(AllowAnonymous::class);
                if(!empty($mAllowAnonymous)) {
                    $mAuthorizeDef = [
                        'allowedAnonymous' => true
                    ];
                }
                
                $m['authorize'] = $mAuthorizeDef;                

                $rule['methods'][] = $m;
            }

            // Add rule to result
            $rules[] = $rule;
        }

        $this->cache->save($cacheKey, $rules);

        return $rules;
    }

    /**
     * Get controller class match rule
     * 
     * @param \ReflectionClass $clsReflection
     * @return string
     */
    protected function getControllerMatch(\ReflectionClass $clsReflection): string
    {
        // Attribute Route
        $attrs = $clsReflection->getAttributes(Attributes\Path::class);
        if(count($attrs) > 0) {
            return end($attrs)->newInstance()->getPath();
        }

        // Match according to a class name - without "Controller" suffix
        $shortName = $clsReflection->getShortName();
        /*$ctrName = preg_replace_callback(
            '/[A-Z]{1}/U',
            fn($matches) => '-'.Strings::lower($matches[0]),
            lcfirst(str_ends_with($shortName, 'Controller') ? Strings::substring($shortName, 0, -10) : $shortName)
        );*/
        return lcfirst(str_ends_with($shortName, 'Controller') ? Strings::substring($shortName, 0, -10) : $shortName); //$ctrName;
    }

    /**
     * Get controller's method match rule
     * 
     * @param \ReflectionMethod $refMethod
     * @return string
     */
    protected function getControllerMethodMatch(\ReflectionMethod $refMethod): string
    {
        // Attribute Route
        $attrs = $refMethod->getAttributes(Attributes\Path::class);
        if(count($attrs) > 0) {
            return end($attrs)->newInstance()->getPath();
        }

        // Match according to a method name
        $shortName = $refMethod->getShortName();
//        $methodName = preg_replace_callback(
//            '/[A-Z]{1}/U',
//            fn($matches) => '-'.Strings::lower($matches[0]),
//            lcfirst($shortName)
//        );
        
        return lcfirst($shortName); //$methodName;
    }    
    
    

    
    /**
     * Check request syntax
     * 
     * @param \stdClass $request
     * @return bool TRUE = OK, FALSE = invalid request 
     */
    protected function checkRequestSyntax($request): bool
    {
        if(!isset($request['jsonrpc']) || $request['jsonrpc'] != self::JSON_RPC_VERSION) {
            return false;
        }
        if(!isset($request['method']) || !is_string($request['method'])) {
            return false;
        }
        if(isset($request['params'])) {
            if(!is_array($request['params'])) {
                return false;
            }
        }
        return true;
    }
    
    
    // --------------------------------------------------------------------------
    
    
    /**
     * Build error from Exception
     * 
     * @param \Components\JsonRpcServer\ApiException $e
     * @param int|null $id
     */
    protected function buildError(ApiException $e, $id = null)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error' => [
                'code' => $e->getCode(),
                'message' => $e->getMessage().
                    // If Nette Debug mode is ON, show also internal message with detailed error description
                    (\Tracy\Debugger::$productionMode ? '' : ' '.$e->getInternalMessage())
            ],
            'id' => $id
        ];
    }
    
    /**
     * Build error structure
     * 
     * @param int $code
     * @param string $message
     * @param string $internalMessage
     * @param int $id
     * @return array Structure
     */
    protected function buildErrorCodeMessage($code, $message, $internalMessage = NULL, $id = NULL)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error' => [
                'code' => $code,
                'message' => trim(
                        $message.
                        // If Nette Debug mode is ON, show also internal message with detailed error description
                        (\Tracy\Debugger::$productionMode ? '' : ' '.($internalMessage ?? ''))
                    )
            ],
            'id' => $id
        ];
    }
    
    /**
     * Build response
     * 
     * @param mixed $result
     * @param mixed $id
     * @return array Structure
     */
    protected function buildResponse($result, $id)
    {    
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result' => $result,
            'id' => $id
        ];
    }
    
    
    // -------------------------------------------------------------------------
    
    
    /**
     * Send structure to the standard output and exit
     * 
     * @param array|\stdClass $structure Structure
     */
    protected function sendResponse($structure)
    {
        http_response_code(200);
        $this->response->setHeader('Content-Type', 'application/json');
        if($structure !== NULL) {
            print Json::encode($structure);
        }
        exit;
    }

   
    
}