<?php

namespace AbraD450\ApiServer\RPC;

/**
 * API Exception
 */
class ApiException extends \Exception
{
    
    /** An error occurred on the server while parsing the JSON text. */
    const E_PARSE_ERROR = -32700;
    const E_PARSE_ERROR_TEXT = 'Parse error.';
    
    /** The JSON sent is not a valid Request object. */
    const E_INVALID_REQUEST = -32600;
    const E_INVALID_REQUEST_TEXT = 'Invalid request.';
    
    /** The method does not exist / is not available. */
    const E_METHOD_NOT_FOUND = -32601;
    const E_METHOD_NOT_FOUND_TEXT = 'Method does not exist.';
    
    /** Invalid method parameter(s). */
    const E_INVALID_PARAMS = -32602;
    const E_INVALID_PARAMS_TEXT = 'Invalid method parameter(s).';
    
    /** Internal JSON-RPC error.*/
    const E_INTERNAL_ERROR = -32603;
    const E_INTERNAL_ERROR_TEXT = 'Internal server error.';
    
    /**
     * Default messages
     * 
     * @var array
     */
    private static $defaultMessages = [
        self::E_PARSE_ERROR => self::E_PARSE_ERROR_TEXT,
        self::E_INVALID_REQUEST => self::E_INVALID_REQUEST_TEXT,
        self::E_METHOD_NOT_FOUND => self::E_METHOD_NOT_FOUND_TEXT,
        self::E_INVALID_PARAMS => self::E_INVALID_PARAMS_TEXT,
        self::E_INTERNAL_ERROR => self::E_INTERNAL_ERROR_TEXT,
    ];
    
    /**
     * Private message
     * 
     * @var string
     */
    private $internalMessage;
    
    /**
     * Constructor
     * 
     * @param int $code
     * @param string $message API message
     * @param string $internal Internal message
     * @param \Throwable $previous
     */
    public function __construct($code, $message = NULL, $internal = NULL, \Throwable $previous = NULL)
    {
        $m = $message === NULL ? self::$defaultMessages[$code] : $message;
        parent::__construct($m, $code, $previous);
        $this->internalMessage = $internal;
    }
    
    /**
     * Get default message
     * 
     * @param int $code
     */
    public function getDefaultMessage()
    {
        return self::$defaultMessages[$this->code];
    }
    
    /**
     * Get internal message
     * 
     * @return string 
     */
    public function getInternalMessage()
    {
        return $this->internalMessage;
    }
    
}