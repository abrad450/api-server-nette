<?php

namespace AbraD450\ApiServer;

/**
 * Object that can be constructed from API request
 */
interface ApiConstructible
{
    
    /**
     * Static factory used to build the object from API parameter
     * 
     * @param mixed $value
     */
    public static function apiConstruct($value);
    
}


/**
 * @deprecated Use ApiConstructible instead
 */
interface IApiConstructible extends ApiConstructible
{
    
}